PYthon MONitoring Tool
----------------------

The tool was designed for monitoring test benches without relying on the Labview
solution which is expensive and require to learn how to use it.

Pymont aims to bring modularity and ease of use for monitoring simple experiments.

For each new instrument, a minimum of code and knowledge of the instrument is 
required. 

Future developments
-------------------

The last step would be to provide a free image of a raspbian OS with the correct
configuration with the software already installed.

Initially the goal was to provide a tool on Windows with a GUI but this part is
now commented in the code (pymont.py) and could be readapted if future needs
shows that it is needed.

Documentation
-------------

Find more about PYMONT using the documentation: https://benoit.serra.gitlab.io/pymont/