# -*- coding: utf-8 -*-
"""
Created on Wed Mar 25 15:55:34 2020

@author: bserra

v0.1 -
"""

# opcua_server.py --- server to send instruction to pymont
#
# Copyright (C) 2018-2020 Benoit Serra
#
# This file is a program monitoring the instruments connected to it.
# It may be used and modified with no restriction; raw copies as well
# as modified versions may be distributed without limitation.

# IMPORTS
#########################
import sys
import os
sys.path.insert(0, "..")
import logging
import time
import numpy as np
import datetime
import json
import socket

# OPC module
import asyncio
from asyncua import ua, Server
from asyncua.common.methods import uamethod
from asyncua.common import utils
import signal
import classes.alarm_system as pac

# For errors
from visa import VisaIOError

# Custom classes
from classes.pymont_decorators import timeit
from classes.communication_server import CommunicationServer

"""
# TO work with embedded console
try:
    from IPython import embed
except ImportError:
    import code

    def embed():
        vars = globals()
        vars.update(locals())
        shell = code.InteractiveConsole(vars)
        shell.interact()
"""
     
class OPCServer:
    """This OPC server interacts with the communication server
    and with the users"""
    def __init__(self, config_filename):
        # Getting pymont logger
        self.logger = logging.getLogger('opcua_server')
        # Initialize Communication Server
        self.logger.info('Starting communication server ...')
        self.CS = CommunicationServer(config_filename)
        self.loop = asyncio.get_event_loop()
        try:
            self.loop.run_until_complete(self.init_server())
        except AttributeError:
            #self.loop.close()
            sys.exit("Received exit, exiting")
        

    async def init_server(self):
        """"""
        # Print IP address of whatever interface is used to connect to the network
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 80))
        IP_ADDRESS = s.getsockname()[0]
        # Port for OPC server will be 4500
        OPC_PORT = 4500
        
        # Generate endpoint and name of the opc server and create the endpoint
        endpoint = f'opc.tcp://{IP_ADDRESS}:{OPC_PORT}/freeopcua/server/'
        name = "Pymont - OpcUa Async Server"
        
        self.server = Server()
        await self.server.init()
        self.server.set_endpoint(endpoint)        
        # Server informations
        self.logger.info(self.server)
        
        # setup our own namespace, not really necessary but should as spec
        uri = "http://examples.freeopcua.github.io"
        self.idx = await self.server.register_namespace(uri)
        self.objects = self.server.get_objects_node()

        # Using the first data point from the communication server
        # to build the different nodes of the OPC server
        self.logger.info(self.CS.last_point)
        
        # Building the nodes of the OPC server
        await self._build_opcnodes(self.CS.last_point,
                                   self.CS.config_file['GLOBAL_CONFIG']['sampling'])
        signal.signal(signal.SIGINT, self._close)
        self.working=True
        self.count = 0
        self.update_flag = True
        
        # Extract alarms from the configuration file
        self.alarms_conditions = self.CS.config_file['Alarms']
        # Use it to initialize the alarm handler (and alarm objects within)
        self.alarms = pac.alarm_handler(self.alarms_conditions)
        # Why should I do to keep the server alive instead of just
        # using self.server.start()?
        async with self.server:
            while self.working==True:
                samplingNode = self.server.get_node("ns="+str(self.idx)+"; s=SamplingTime")
                samplingTime = float(await samplingNode.read_value())

                if self.update_flag:
                    await asyncio.sleep(samplingTime)
                    await self._update()
                else:
                    await asyncio.sleep(samplingTime)
                    pass

    async def countTasks(self):
      print('Tasks count: ', len(asyncio.Task.all_tasks()))
      print('Active tasks count: ', len(
          [task for task in asyncio.Task.all_tasks() if not task.done()])
      )

    @uamethod
    async def connect_instrument(self, parent, instrument_tag):
        if instrument_tag in self.list_instruments:
            self.CS._connect_instrument(instrument_tag)
            reply = "Connected "+instrument_tag
        else:
            reply = "Ressource not available. Failed to connect "+instrument_tag
        return reply

    @uamethod
    async def disconnect_instrument(self, parent, instrument_tag):
        if instrument_tag in self.list_instruments:
            self.CS._disconnect_instrument(instrument_tag)
            reply = "Disconnected "+instrument_tag
        else:
            reply = "Ressource not available. Failed to disconnect "+instrument_tag
        return reply

    @uamethod
    async def reconnect_instrument(self, parent, instrument_tag):
        if instrument_tag in self.list_instruments:
            self.CS._disconnect_instrument(instrument_tag)
            time.sleep(1)
            self.CS._connect_instrument(instrument_tag)
            time.sleep(1)
            reply = "Reconnected "+instrument_tag
        else:
            reply = "Ressource not available. Failed to reconnect "+instrument_tag
        return reply
        
    @uamethod
    async def get_lastpoint(self, parent):
        return str(self.CS.last_point)
    
    @uamethod
    async def switch_monitoring(self, parent):
        self.update_flag = not self.update_flag
        if self.update_flag:
            response = 'Monitoring started.'
        else:
            response = 'Monitoring stopped.'
            
        return response
        
    @uamethod
    async def send_command(self, parent, string):
        """send_command will format the command and send it to the client
        Send a command to the TAG instrument, saving it to the log file at
        the same time with a time stamp"""
        command = [i.split('|') for i in string.split(';')]
        for i in command:
            self.logger.info('Commands '+' '.join(i))
            if i[0] not in self.CS.instruments_tag.keys():
                response = i[0]+': is not a valid instrument'
                self.logger.warning(response)
            else:
                response = None
                self.logger.info(i[0]+i[1])
                try:
                    self.logger.info('Command sent '+i[1].replace('\r\n',''))
                    # Works for all but will not for user defined command
                    response = self.CS.instruments_tag[i[0]].instrument.query(i[1].replace('\r\n',''))
                    # Should work, but need to include _send_command for everything
                    # response = self.CS.instruments_tag[i[1]]._send_command(i[2].replace('\r\n',''))
                    self.logger.info('Response '+i[1].replace('\r\n','')+':'+str(response))

                except VisaIOError as err_string:
                    self.logger.info('No response from instrument ['+str(err_string)+']')
                    response = 'Send '+i[1].replace('\r\n','')+' to '+i[0]
                #self.send_client(client, client_request.encode(), json.dumps(response))
                command_array = await self.myarrayvar.read_value()
                new_string = command_array[0]+':'+str(time.time())+';'+string

                await self.myarrayvar.set_value([new_string])#command_array)
        return json.dumps(response)
        
    @uamethod
    def reload_config(self, parent):
        """Stop the opc server and rebuild it with a possible new configuration if instruments have been disconnected"""
        self.working = False
        self.server.close()
        self.loop = asyncio.get_event_loop()
        self.loop.run_until_complete(self.init_server())
        self.loop.close()
        return None

    # @uamethod
    # def get_instruments(self):
    #     response = dict()
    #     for inst in self.CS.instruments_tag.keys():
    #         response[inst] = str(type(self.CS.instruments_tag[inst]))
    #     self.send_client(client, data, json.dumps(response))

    # @uamethod
    # def set_savepath(self, savepath:str):
    #     return None

    @uamethod
    async def start_monitoring(self, *args):
        self.monitoring_loop.start()
        while self.monitoring_status is True:
            await self._update()
            asyncio.sleep(5)

    @uamethod
    def _close(self):
        self.working=False

    @uamethod
    def stop_monitoring(self):
        self.monitoring_loop.stop()

    # @timeit
    async def _update(self):
        #await self.countTasks()
        t_start = time.time()
        self.count +=1
        read = self.CS._read_setup()
        self.CS.last_point = read
        self.alarms.evaluate_datapoint(read)
        #try:
        #    self.alarms.evaluate_datapoint(read)
        #except:
        #    self.logger.info('Alarms crashed')

        # Here I need to loop on values from the read and not the opc server
        # that way if a value is missing it is just not updated (and maybe tagged?)
        for node in self.list_nodes:
            # For each variable node that was created in _build_opcnodes
            variable_tag = (await node.read_browse_name()).to_string().split(':')[1]
            #print (type(read[instrument_tag]))
            # the variable returned for the read of all instrument
            # is a list with [0] being the value, [1] being the comments
            try:
                # Set the value with what was transmitted to the communication server (self.CS)
                await node.set_value(read[variable_tag][0])
                # Check if value this variable is defined in alarm conditions
                #if instrument_tag in self.alarms_conditions.keys():
                #    # Check if the alarm is enabled
                #    if self.alarms_conditions[instrument_tag]['enabled']:
                #        # Check if alarm trigger
                #        self.alarms.evaluate_datapoint(read, instrument_tag)
            except:
                self.logger.warning('Variable '+variable_tag+' not available')
                await node.set_value('NaN')
                
        #for node in self.other_fields:
        #    # For each variable node that was created in _build_opcnodes
        #    field_name = (await node.read_browse_name()).to_string().split(':')[1]
        #    await node.set_value(read[field_name][0])
        timing = str(time.time() - t_start)
        timing_node = self.server.get_node("ns="+str(self.idx)+"; s=ReadTime")
        sampling_node = self.server.get_node("ns="+str(self.idx)+"; s=SamplingTime")
        await timing_node.set_value(timing)
        return None

    async def _build_opcnodes(self, data_model, samplingTime=30):
        """"""
 
        # Use the data point to list how many instrument there are
        # by extracting the instrument tags (TC001, P001, ...)
        # extract instrument name from configuration and put it as a variable node of the OPC
        self.benchname = self.CS.config_file['GLOBAL_CONFIG']['benchname']
        bench_variable = await self.objects.add_variable(self.idx, 'benchname', self.benchname)
        self.list_instruments = np.unique([i.split('_')[0] for i in data_model.keys() if i not in ['Comments', 'Time']])
        list_others = ['Comments', 'ReadTime', 'SamplingTime']
        # Create an Instrument object that will group all instrument (no comments/time)
        self.instruments = await self.objects.add_object(self.idx, 'Instruments')
        # Saving time and comment in the Objects node
        self.other_fields = [await self.objects.add_variable("ns="+str(self.idx)+"; s="+other, other, '') for other in list_others]
        sampling_time = self.server.get_node("ns="+str(self.idx)+"; s=SamplingTime")
        await sampling_time.set_value(samplingTime)
        # For each instrument, create an object to contain the variables
        self.list_objects = [await self.instruments.add_object(self.idx, instrument) for instrument in self.list_instruments]
        # Create an object to store the methods of the OPC server
        #self.test_object = await self.objects.add_object("ns="+str(self.idx)+"; s=internalMethods", "InternalMethods")
        self.test_object = await self.objects.add_object("ns="+str(self.idx)+"; s=internalMethods", 'InternalMethods')
        # Create an empty list that will contain every node of the variables
        self.list_nodes = list()

        # First creating the nodes for the functions (to keep same ids)
        # Building node for functions
        # Output argument for functions in OPC server??
        outarg = ua.Argument()
        outarg.Name = "Result"
        outarg.DataType = ua.NodeId(ua.ObjectIds.String)
        outarg.ValueRank = -1
        outarg.ArrayDimensions = []
        outarg.Description = ua.LocalizedText("Result from function")
        # Output argument for functions in OPC server??
        outarg2 = ua.Argument()
        outarg2.Name = "Result command"
        outarg2.DataType = ua.NodeId(ua.ObjectIds.String)
        outarg2.ValueRank = -1
        outarg2.ArrayDimensions = []
        outarg2.Description = ua.LocalizedText("Result2 from function")
        # Input argument for functions in OPC server??
        inarg = ua.Argument()
        inarg.Name = "Input string"
        inarg.DataType = ua.NodeId(ua.ObjectIds.String)
        inarg.ValueRank = -1
        inarg.ArrayDimensions = []
        inarg.Description = ua.LocalizedText("Input string for function")

        # Creating a node where commands will be stored
        commands_variable = await self.objects.add_object(self.idx, 'IssuedCommands')
        self.myarrayvar = await commands_variable.add_variable("ns="+str(self.idx)+"; s=Commands", "Commands", ['Timestamp;StringReceived'])
        #print (self.myarrayvar, type(self.myarrayvar))
        #self.myarrayvar = await commands_variable.add_variable(self.idx, "myStronglytTypedVariable", ua.Variant([], ua.VariantType.UInt32))
        myprop = await self.objects.add_property(self.idx, "configFile", "I am a property")

        #update_node1 = self.test_object.add_method(self.idx, "start monitoring", self.start_monitoring)
        #update_node2 = self.test_object.add_method(self.idx, "stop monitoring", self._close)
        update_node3 = await self.test_object.add_method("ns="+str(self.idx)+"; s="+"lastDatapoint", "get last datapoint", self.get_lastpoint, [], [outarg])
        update_node4 = await self.test_object.add_method("ns="+str(self.idx)+"; s="+"sendCommand", "send command", self.send_command, [inarg], [outarg2])
        update_node5 = await self.test_object.add_method("ns="+str(self.idx)+"; s="+"disconnectInstrument", "disconnect instrument", self.disconnect_instrument, [inarg], [outarg2])
        update_node6 = await self.test_object.add_method("ns="+str(self.idx)+"; s="+"connectInstrument", "connect instrument", self.connect_instrument, [inarg], [outarg2])
        update_node7 = await self.test_object.add_method("ns="+str(self.idx)+"; s="+"reconnectInstrument", "reconnect instrument", self.reconnect_instrument, [inarg], [outarg2])
        update_node8 = await self.test_object.add_method("ns="+str(self.idx)+"; s="+"switchMonitoring", "stop/start monitoring updates", self.switch_monitoring, [], [outarg])

        # Iterate over the instruments
        for instrument_object in self.list_objects:
            # get_browse_name (opcua) / read_browse_name (asyncua) returns 
            # a QualifiedName that you need to stringify then splitting 
            # the string (For ex. 2:T001) to get he instrument tag.
            # Can check in asyncua/common/node.py
            instrument_tag = (await instrument_object.read_browse_name()).to_string().split(':')[1]
            # For each entry in the read of all instrument
            for keyw, value in data_model.items():
                # If this instrument got variable recorded
                if instrument_tag in keyw:
                    # If this is an instrument (not Time, Comments)
                    # create a variable associated with this node
                    # new_node = await instrument_object.add_variable(self.idx, keyw, value)
                    new_node = await instrument_object.add_variable("ns="+str(self.idx)+"; s="+keyw, keyw, value[0])
                    # and append it to the list
                    self.list_nodes.append(new_node)

        return None

#    async def __enter__(self):
#        """
#        __enter__ and __exit__ used for ease of use when using 'with x as y'
#        https://stackoverflow.com/questions/1984325/explaining-pythons-enter-and-exit
#        """
#        await self.server.start()
#        print ('Server started')
#        return self.server

#    async def __exit__(self, exc_type, exc_val, exc_tb):
#        await self.server.stop()


if __name__ == "__main__":

    script_dir = os.path.dirname(__file__)

    data_model = dict({"T001_A":300,
                       "T001_B":300,
                       "T001_C":300,
                       "T002_C":300,
                       "T002_D":300,
                       "P001_Pressure":1e-4,
                       "C001_Pressure":300,
                       "C001_WaterInlet":300,
                       "C001_WaterOutlet":300})

    with InstructionServer(
            "opc.tcp://0.0.0.0:40840/freeopcua/server/",
            "FreeOpcUa Example Server",
            data_model) as server:
        embed()
