# -*- coding: utf-8 -*-
"""
Created on Mon Sep  7 13:50:36 2020

@author: bserra
"""
import logging
import time
import numpy as np

from .send_mail import mailing
from .send_sms import sms_eagle

def check_alarm(alarm_type: str, conditions: dict, value: int):
    """Takes a type of alarm and the value and will check if it activates"""
    def inbounds(conditions, value,
                 alarm_activated=False):
        """ Is the value within defined bounds """
        if conditions['low'] <= value <= conditions['high']:
            alarm_activated = True
        return alarm_activated

    def outbounds(conditions, value,
                  alarm_activated=False):
        """ Is the value outside of defined bounds """
        if (conditions['low'] >= value) | (value >= conditions['high']):
            alarm_activated = True
        return alarm_activated

    def status(conditions, value,
               alarm_activated=False):
        """ Is the value equals to the defined condition """
        if value == conditions:
            alarm_activated = True
        return alarm_activated

    def timeout(conditions, value,
                alarm_activated=False):
        """ Is 'Timeout' inside of the value string """
        if 'Timeout' in value:
            alarm_activated = True
        return alarm_activated

    alarm_dictionnary = {'inbounds':inbounds,
                         'outbounds':outbounds,
                         'status':status,
                         'timeout':timeout}

    return alarm_dictionnary[alarm_type](conditions, value)


def template_alarm(alarm_type):
    """This is a model alarm that I would like to pass as a decorator"""
    def alarm_actions(self, *args, additional_infos=None):
        """This is what happens when an alarm is triggered"""
        self.message = "PAS: "+str(alarm_type)+" "
        self.logger.info(self.message)
        return alarm_type(self)

    return alarm_actions

"""
alarms_message = {'inbounds'  : {'message': 'Inbound alarm',
                                 'function': inbounds},
                  'outbounds' : {'message': 'Outbound alarm'},
                  'status'    : {'message': 'Status alarm'},
                  'timeout'   : {'message': 'Timeout alarm'}
                  }
"""
# CLASSES
class alarm:
    def __init__(self, variable_tag, alarm_parameters):
        """This is the alarm object which defines how the alarm will work"""
        # Enabled?
        self.enabled = alarm_parameters['enabled']
        # Name of the alarm
        if alarm_parameters['name'] == '':
            self.name = variable_tag
        else:
            self.name = str(alarm_parameters['name'])
        self.variable = variable_tag
        # Type of alarm: inbounds/outbounds/status/timeout
        self.type = alarm_parameters['type']
        # Parameter of the alarm type
        self.parameter = alarm_parameters['params']
        # Custom message to add when sending info about the alarm
        self.message = alarm_parameters['message']
        # Time since trigger of the alarm
        self.time_since_trigger = time.time()
        # Timestamp (or creation) of the alarm
        #self.timestamp = 
        # Number of time it has been triggered
        self.counter = 0

        # Template message
    def _trigger(self, datapoint):
        """"""
        return None

class alarm_handler:
    """Class to handle alarms"""
    def __init__(self, alarm_dict):
        """Docstring"""
        # Logger
        self.logger = logging.getLogger('alarm_handler')
        
        # Load alarm condition from the .yml configuration file
        self.alarms_conditions = alarm_dict
        
        # Initialize mailer and sms sender
        self.sms = sms_eagle(alarm_dict['Options']['sms'])
        self.mail = mailing(alarm_dict['Options']['mailer'])
        
        # Create a dictionnary containing all the alarm with the variable tag as the key
        self.alarms = {}
        del alarm_dict['Options']
        for variable_tag, alarm_definition in alarm_dict.items():
            self.alarms.update({variable_tag: alarm(variable_tag, alarm_definition)})
        
        self.logger.info(self.alarms)

    def evaluate_datapoint(self, read):
        """This method take the last datapoint and the defined alarm, check the boundaries and check possible errors"""
        # For each entry in the read of all instrument
        for variable_tag, alarm in self.alarms.items():
            #print (alarm.enabled)
            if alarm.enabled:
                #print (check_alarm(alarm.type, alarm.parameter, float(read[variable_tag][0])))
                if check_alarm(alarm.type, alarm.parameter, float(read[variable_tag][0])):
                    alarm.counter += 1
                    #print (time.time() - alarm.time_since_trigger)
                    if time.time() - alarm.time_since_trigger < 10*60:
                        #print ('send message')
                        alarm.time_since_trigger = time.time()
                        self.sms.send_sms('+33669712393', alarm.message)
                        self.mail.send_mail('bserra@eso.org', variable_tag+alarm.type, alarm.message)

    def alarms_report(self):
        """Create an alarm report that is send every 12h"""
