# -*- coding: utf-8 -*-
"""
Created 06/10/2020

Using the smtpauth server from ESO, sending a mail to someone using
smtplib

"""
import logging

class mailing:
    """Snippet of a class using the credential from the SMS Eagle portal
    to send an SMS."""

    def __init__(self, mail_options):
        """Initialising logger and loading info from init the class"""
        self.logger = logging.getLogger('mail_alarms')
        self.server = mail_options['server']
        self.sender = mail_options['from']
        self.login = mail_options['auth_login']
        self.passw = mail_options['auth_passw']
        self.logger.info('mailer infos loaded')
        
    def send_mail(self, recipient, subject, message):
        # Necessary to properly parse/concatenate argument into a PHP 
        # request
        """Send mail from a message"""

        import smtplib
        from email.mime.multipart import MIMEMultipart
        from email.mime.text import MIMEText
        
        mail_args = { 'login':     self.login, 
                      'pass':      self.passw, 
                      'from':      self.sender,
                      'to':        recipient,
                      'subject':   subject,
                      'message':   message }
                      
        self.logger.info(str(mail_args))
        
        # Send mail flag initialised to False
        mailsend = False
        # Creating the message
        msg = MIMEMultipart()
        # From, To and Subject
        msg['From'] = self.sender
        msg['To'] = recipient
        msg['Subject'] = subject
        # Attach the message
        msg.attach(MIMEText(message))

        # Now that the message is created, it needs to be send
        # Creating SMTP interface
        server = smtplib.SMTP()
        server.set_debuglevel(0)
        # Connecting to the server, port 25
        server.connect(self.server,25)
        # Necessary
        server.ehlo()
        server.starttls()
        server.ehlo()
        # Sometimes the authentification does not go smoothly, so until
        # the message is send, try every 5 seconds
        while mailsend == False:
            try:
                server.login(self.login, 
                             self.passw)
                result = server.sendmail(self.sender, recipient, msg.as_string())
                mailsend = True
                server.quit()
            except smtplib.SMTPAuthenticationError as exc:
                print ('Authentication error, sending the mail in 5 seconds')
                time.sleep(5)

        self.logger.info(result)

if __name__ == "__main__":
    # This allows for basic information logging to be displayed on screen
    logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)
    
    # Information of the smtpauth server to connect
    infos = {'server':      'smtpserver',
             'from':        'mail address that sends the message',
             'auth_login':  'username',
             'auth_passw':  'password'}
             
    test_mail = mailing(infos)
    # Mail address / Subject / Message content
    test_mail.send_mail('recipient of the mail', 'subject of the message', 'Message to send')

