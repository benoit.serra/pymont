# -*- coding: utf-8 -*-
"""
Created on Tue Apr  30 12:05:00 2019

@author: serra

v1.2 -
issue 3 - Possibility to disconnect one instrument from the actual config
issue 3 - When all instruments disconnected, can change the config file
Logger used for everything
Mailer in case of problem?

Changing software architecture with separate classes

"""

# pymont.py --- Simple application for monitoring Cryostats
#
# Copyright (C) 2018-2019 Benoit Serra
#
# This file is a program monitoring the instruments connected to it.
# It may be used and modified with no restriction; raw copies as well
# as modified versions may be distributed without limitation.

# IMPORTS
#########################
from __future__ import unicode_literals
import sys
import os
import time, datetime
import itertools
import logging

import numpy as np
#import pyqtgraph as pg
from collections import defaultdict

import drivers.lakeshore.lakeshore as ls
import drivers.pfeiffer.vacuumgauge as vg
import drivers.roheandschwarze.roheandschwarze as rs
import drivers.rigol.rigol as rig
import drivers.agilent.agilent as agi
import drivers.riello.ups_driver as riello
import drivers.sumitomo.sumitomo as sumitomo
import drivers.cisystem.blackbody as cibb
import drivers.jumo.imago as jumo
import drivers.schneider.schneider as sch
import drivers.hgh.hgh as hgh
import drivers.uniblitz.uniblitz as unib
import drivers.infraredsystemsdevelopment.blackbody as isdbb
import drivers.edwards.vacuumgauge as vg_edw
import drivers.alcatel.alcatel as alc
import drivers.endresshauser.picomag as wfm
import drivers.keysight.keysight as ks
import drivers.keithley.electrometer as kem
import drivers.newport.power_supply as npsu
import drivers.oriel.monochromator as omon
import drivers.cryomech.cryomech as cm
import drivers.faulhabner.motors as fhb
import drivers.pce.pce as pce
import drivers.leybold.coolpak as cpk
import drivers.ifm.sbg as sbg

import visa
import socket
import threading
import json
import yaml

import glob


# METHODS
#########################
def select_instrument(identifier):
    """"""
    instruments = {'Lakeshore218':ls.LakeShore218,
                   'Lakeshore336':ls.LakeShore33x,
                   'Lakeshore340':ls.LakeShore34x,
                   'HMC8012':rs.HMC8012,
                   'HMP4040':rs.HMP4040,
                   'HMP4030':rs.HMP4030,
                   'DP831A':rig.DP831A,
                   'TPG262':vg.TPG262,
                   'TPG362':vg.TPG362,
                   'DSO5034':agi.DSO5034A,
                   'U3606A':agi.U3606A,
                   'DG1062Z':rig.DG1000Z,
                   'DM3068':rig.DM3068,
                   'UPS_VSD3x':riello.UPSVSD3xxx,
                   'COMP_F70':sumitomo.COMP_F70,
                   'SR80':cibb.SR80,
                   'SR20':cibb.SR20,
                   'SR800R':cibb.SR800R,
                   'Imago500':jumo.imago500,
                   'FilterWheel':sch.FilterWheel,
                   'ECN100':hgh.ECN100,
                   'IR301':isdbb.IR301,
                   'ADCMKII':vg_edw.vacgauge,
                   'ATH400M':alc.ATH400M,
                   'Picomag':wfm.picomag,
                   'VED24':unib.VED24,
                   '33600A':ks.WG33600A,
                   'KE6514':kem.KE6514,
                   'DR69931':npsu.DR69931,
                   'MS257':omon.MS257,	
                   'CPA289C':cm.CPA289C,
                   'CPA286I':cm.CPA286I,
                   'MCDC3002':fhb.MCDC3002,
                   'PCE-T395':pce.T395,
                   'CPK6000':cpk.Coolpak6000,
                   'SBG232':sbg.sbg232}

    return instruments[identifier]

def stop_communication(CommServ):
    """"""
    CommServ.close()

class CommunicationServer(object):
    """Class for starting/stopping communication with the instruments
       Maybe add a separate communication log?
    """
    def __init__(self, config_filename, time_fmt='%Y-%m-%d_%H-%M-%S'):
        """Initialization of the communication server"""
        # Getting pymont logger
        self.logger = logging.getLogger('pymont')
        self.config_filename = config_filename
        self.config_file = self._load_configuration('config/'+config_filename)
        # Time between measures
        self.dt = self.config_file["GLOBAL_CONFIG"]["sampling"]
        # Extract the instrument configuration from the config file
        dict_config = self.config_file["Instruments"]

        self.filename = time.ctime()
        # Starting pyvisa ResourceManager
        self.inst_rm = visa.ResourceManager()
        self.instruments = defaultdict(list)
        self.instruments_tag = dict()

        # Config file timeout in seconds, in pyvisa it is in ms
        timeout = int(self.config_file["GLOBAL_CONFIG"]["timeout"])*1000


        self.connection_infos = {}

        # For each type of instrument defined in the json file
        for instrument_type in dict_config:
            # Get its tag
            instrument_tag = dict_config[instrument_type]
            # For each instrument of this type, connect to it
            for idx,protocol in enumerate(instrument_tag['comm']):
                instrument = select_instrument(instrument_tag['ident'][idx])
                config = instrument_tag['config'][idx]
                models = instrument_tag['models']
                # gives ID_STR everytime: T001, T002 etc...
                id_str = instrument_type+str('{:03d}'.format(idx+1))
                self.logger.debug(str(instrument)+' - '+id_str)

                conn = instrument(RESOURCE_STRING   = protocol,
                                  RESOURCE_MANAGER  = self.inst_rm,
                                  RESOURCE_ID       = id_str,
                                  RESOURCE_TIMEOUT  = timeout,
                                  RESOURCE_CONFIG   = config,
                                  RESOURCE_MODEL   = models)
                self.connection_infos.update({id_str:{'driver': instrument,
                                                      'infos':{'RESOURCE_STRING'   : protocol,
                                              'RESOURCE_MANAGER'  : self.inst_rm,
                                              'RESOURCE_ID'      : id_str,
                                              'RESOURCE_TIMEOUT'  : timeout,
                                              'RESOURCE_CONFIG'   : config,
                                              'RESOURCE_MODEL'   : models}}})

                if conn.connected == True:
                    self.instruments_tag[id_str] = conn
                    if dict_config[instrument_type]['type'] in self.instruments.keys():
                        self.instruments[dict_config[instrument_type]['type']].append(self.instruments_tag[id_str])
                    else:
                        self.instruments[dict_config[instrument_type]['type']] = [self.instruments_tag[id_str]]
                else:
                    self.logger.warning('Instrument '+str(instrument)+' - '+id_str+' - '+str(protocol)+ ' Failed to connect.')


        #self.logger.info('Connected instruments:',str(self.instruments_tag))
        self.df = time_fmt
        self._connected = True
        self.logger.info('Connexion initialization done.')
        self.last_point = self._read_setup()

    def _load_configuration(self, config_file:str):
        with open(r''+config_file) as file:
            # The FullLoader parameter handles the conversion from YAML
            # scalar values to Python the dictionary format
            config_file = yaml.load(file, Loader=yaml.FullLoader)
        return config_file

    def _disconnect_instrument(self, instrument_tag):
        """"""
        self.logger.info('Disconnecting '+instrument_tag)
        self.instruments_tag[instrument_tag].instrument.close()
        self.instruments_tag[instrument_tag].connected = False

    def _connect_instrument(self, instrument_tag):
        """"""
        self.logger.info('Connecting '+instrument_tag)
        self.instruments_tag[instrument_tag].__init__(**self.connection_infos[instrument_tag]['infos'])
        self.instruments_tag[instrument_tag].connected = True

    def _read_UPSstat(self):
        """"""
#        battery_status = {**self.ups.dump_sensors()}
        return None

    def _read_setup(self):
        """Loop over the connected instrument and return the data:
        Input: None
        Output: dictionary with <tag>_<variable>:(<value>, <comment>)

        <tag>: created at the connection, identify the instrument (T001 for ex)
        <variable>: variable name as defined in the driver
        <value>: value formatted as defined in the driver
        <comment>: string to comment what is send as defined in the driver
        """
        results = dict()
        time = datetime.datetime.utcnow().strftime(self.df)
        results.update(Time=(time,'[str] Date with UTC time'))
        results.update(Comments=["","[str] Commments on the measure"])
        for key in self.instruments_tag.keys():
            if self.instruments_tag[key].connected == True:
                try:
                    instrument_reading = self.instruments_tag[key].dump_sensors()
                    # If there is a names field in the yaml file for this instrument type
                    if 'names' in self.config_file["Instruments"][key[:-3]]:
                        replacement_names = self.config_file["Instruments"][key[:-3]]['names']
                        # Get keys
                        oldkeys = list(instrument_reading.keys())
                        # Change "item" to "object"
                        newkeys = list()
                        for s in oldkeys:
                            if s in replacement_names.keys():
                                newkeys.append(replacement_names[s])
                            else:
                                newkeys.append(s)
                        # Get values
                        vals = list(instrument_reading.values())
                        # Create new dictionary by iterating over both newkeys and vals
                        instrument_reading = {k: v for k, v in zip(newkeys, vals)}
                    else:
                        instrument_reading = instrument_reading

                    results.update(instrument_reading)


                except Exception as e:
                    # If an intrument takes to much time to respond, timeout
                    # Send a dictionnary with the error the tag
                   self.logger.debug('Instrument '+str(self.instruments_tag[key])+' read error')
                   results['Comments'][0]+='Error instrument - '+str(key)+\
                                     ' '+str(self.instruments_tag[key])+\
                                     ' '+str(e)

        return results

    def _save(self):
        """"""
        self.save_file.flush()
        os.fsync(self.save_file)

    def start(self, path=''):
        try:
            start_time = datetime.datetime.utcnow().strftime(self.df)
            self.save_file = open(path+\
                                  start_time+'.txt','w')
            self.header = self._read_setup().keys()
            self.save_file.write('Reading #\t'+\
                                 '\t'.join(self.header)+'\tComments\n')
        except:
            self.cleanup()

    def stop(self):
        """"""
        self._connected = False
        for inst_tag in self.instruments_tag.keys():
            self.logger.info('Closing connection to '+str(inst_tag)+' - '+str(self.instruments_tag[inst_tag]))
            self.instruments_tag[inst_tag].instrument.close()
        self.inst_rm.close()

# MAIN
#########################
if __name__ == "__main__":
    print ('Nothing to do ...')

# GARBAGE
#########################
