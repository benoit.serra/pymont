import time
import logging

def timeit(method):
    def timed(*args, **kw):
        logger = logging.getLogger(str(method.__name__))
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
            print (int((te - ts) * 1000))
        else:
            timing = '%r  %2.2f ms' % \
                  (method.__name__, (te - ts) * 1000)
            print (timing)
        logging.info(timing)
        return result
    return timed