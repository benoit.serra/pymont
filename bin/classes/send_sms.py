# -*- coding: utf-8 -*-
"""
Created 06/10/2020

IT has a device named 'SMS Eagle' that can send SMS through a web portal
this snippet is using infos to concatenate the necessary url using
urllib parse/request (PHP). 

"""
import logging

class sms_eagle:
    """Snippet of a class using the credential from the SMS Eagle portal
    to send an SMS."""

    def __init__(self, sms_options):
        """Initialising logger and loading info from init the class"""
        self.logger = logging.getLogger('sms_alarms')
        self.base_url = sms_options['base_url']
        self.login = sms_options['auth_login']
        self.passw = sms_options['auth_passw']
        self.logger.info('sms_eagle infos loaded')
        
    def send_sms(self, recipient, message):
        # Necessary to properly parse/concatenate argument into a PHP 
        # request
        import urllib.parse
        import urllib.request
        """Send SMS from a message"""
        query_args = { 'login':     self.login, 
                       'pass':      self.passw, 
                       'to':        recipient, 
                       'message':   message }
        self.logger.info(str(query_args))
        encoded_args = urllib.parse.urlencode(query_args)
        url = self.base_url + '?' + encoded_args
        result = urllib.request.urlopen(url).read()
        self.logger.info(result)

if __name__ == "__main__":
    # This allows for basic information logging to be displayed on screen
    logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)
    
    # Information of the SMS Eagle web portal, port is usually 80
    infos = {'base_url':    'http://IPv4:port/index.php/http_api/send_sms',
             'auth_login':  'username',
             'auth_passw':  'password'}
             
    test_sms = sms_eagle(infos)
    # phone number should include the prefix + if sms is send outside of the
    # country
    test_sms.send_sms('phonenumber', 'Message to send')
