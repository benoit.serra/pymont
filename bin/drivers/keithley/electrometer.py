# -*- coding: utf-8 -*-
"""
Created on Wed May  2 17:45:05 2018

@author: bserra

Problem with no backend avalables
https://stackoverflow.com/questions/13773132/pyusb-on-windows-no-backend-available?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
add filter to usb serial device
"""

import json
import time
from numpy import size, unique
import sys

# Code translations constants
EM_MODES = {
    'VOLTAGE': ('CONF:VOLT','[V]'),
    'CURRENT': ('CONF:CURR','[A]')
}

#### Keithley ELectrometer 6514 #################################################################

class KE6514(object):
    r"""Class that implements the common driver for the 6514 electrometer.
    """

    LF = chr(10)

    def __init__(self, 
                 RESOURCE_STRING, 
                 RESOURCE_MANAGER = None,
                 RESOURCE_ID = '',
                 TERMINATION_STRING = '\n',
                 RESOURCE_TIMEOUT = 5,
                 **kwargs):
        
        """Initialize internal variables and ethernet connection

        :param RESOURCE_STRING: The adress of the agilent
        :type RESOURCE_STRING: str
        :param TERMINATION_STRING: '\n' by default for agilent
        :type TERMINATION_STRING: str
        """
        try:
            self.instrument = RESOURCE_MANAGER.open_resource(RESOURCE_STRING,
                                                             open_timeout = RESOURCE_TIMEOUT)
            self.instrument.read_termination = TERMINATION_STRING
            self.instrument.write_termination = TERMINATION_STRING
            self.instrument.timeout = RESOURCE_TIMEOUT
            self.connected = True
        except:
            self.connected = False

        try:
            self.inst_id = RESOURCE_ID+'_'
        except:
            self.inst_id = ''
        
        self.mode = kwargs['RESOURCE_CONFIG']
        if RESOURCE_MANAGER == None:
            sys.exit('No VISA resource manager found')
        if self.connected:
            self.instrument.write(EM_MODES[self.mode][0])
    
    def __format_reading(self, reading):        
        """Readings of the Keithley are in the following format:
        Reading, Timestamp, Status
        """
        format = ['Reading-'+self.mode, 'Timestamp', 'Status']
        formatted_reading = [(format[idx], value) for idx, value in enumerate(reading.split(','))]
        return formatted_reading
    
    def identify(self):
        return self.instrument.query('*IDN?')
    
    def fetch(self):
        """Fetch the latest data point

        :param command: The command to send
        :type command: str
        :raises IOError: if the negative acknowledged or a unknown response
            is returned
        """   
        results = dict()
        fetch_value = self.instrument.query('FETCh?')
        fetch_formatted = self.__format_reading(fetch_value)
 
        for format, value in fetch_formatted:
            results.update({self.inst_id+self.mode+'_'+format[:4]:[value, format]})
        
        return results

    def read(self):
        """Fetch the latest data point

        :param command: The command to send
        :type command: str
        :raises IOError: if the negative acknowledged or a unknown response
            is returned
        """   
        results = dict()
        fetch_value = self.instrument.query('READ?')
        fetch_formatted = __format_reading(results)
        print (fetch_formatted)
        for format, value in fetch_formatted:
            results.update({self.inst_id+self.mode+'_'+format[:4]:[value, format]})
        
        return results
        
    def dump_sensors(self):
        """"""
        results = {**self.fetch()}

        return results
