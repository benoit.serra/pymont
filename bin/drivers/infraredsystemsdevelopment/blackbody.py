# -*- coding: utf-8 -*-
"""
Created on Mon Jan 28 12:19:21 2019

@author: bserra
"""
# IMPORTS
#########################
import json
import sys

import time
import minimalmodbus

# METHODS 
#########################

#### IR301 ################################################################

class IR301(object):
	r"""Class that implements the common driver for the TeePee 
	(Imago500) measurement and control unit.
	This instrument is controlled through the MODBUS interface (RS422)
	so we can't use pyvisa 
	"""
	def __init__(self,
				RESOURCE_STRING, 
				RESOURCE_MANAGER = None,
				RESOURCE_ID = '',
				RESOURCE_TIMEOUT = 5,
				TERMINATION_STRING = chr(13)+chr(10),
				**kwargs):     
		"""Initialize internal variables and ethernet connection

		:param RESOURCE_STRING: The adress of the Lakeshore
		:type RESOURCE_STRING: str
		:param TERMINATION_STRING: '\r\n' by default for Pfeiffer gauges
		:type TERMINATION_STRING: str
		"""
		# Important for enabling disconnection/connection.
		# if this is not here, the minimalmodbus library will display
		# ' Attempting to use a port that is not open' after reconnect
		minimalmodbus.CLOSE_PORT_AFTER_EACH_CALL = True

		comm_infos = RESOURCE_STRING.split('::')
		try:
			self.instrument = test(comm_infos[0].replace('MODBUS',''), int(comm_infos[1]), mode = 'rtu')
			self.instrument.serial.baudrate = 19200
			self.instrument.serial.bytesize = 8
			self.instrument.serial.parity   = 'N' # serial.PARITY_NONE
			self.instrument.serial.stopbits = 1
			self.instrument.serial.timeout  = RESOURCE_TIMEOUT   # seconds	            
			self.connected = True
		except:
			self.connected = False

		try:
			self.inst_id = RESOURCE_ID+'_'
		except:
			self.inst_id = ''
		
		if RESOURCE_MANAGER == None:
			sys.exit('No VISA resource manager found')

	def _send_command(self,
					address,
					desc):
		""""""
		if isinstance(address,int):
			register_modetest = 4
			register_numDec = 0
			reply = self.instrument.read_register(address, register_numDec, register_modetest)

		return reply

	def dump_sensors(self):
		""""""
		results = dict()
		registers = dict({'Setpoint':100,
						  'Temperature':300})
		for keyw in registers:
			result = self._send_command(registers[keyw], keyw)
			results.update({self.inst_id+keyw: (str(result), ' [x10 Celsius]')})
		return results

class test(minimalmodbus.Instrument):

	r"""Class inheriting from minimalmodbus.Instrument to add close and query methods needed for
	pymont.
	"""
	def __init__(self, portname, slaveaddress, mode= 'rtu'):
		super(test,self).__init__(portname, slaveaddress, mode= mode)

	def close(self):
		r"""Method to close the communication with the JUMO using the 
		CLOSE methods defined in pymont
		"""
		self.serial.close()

	def query(self,
			  command):
		r"""Method to query for registers using the QUERY method in
		the instruction server in pymont
		"""
		if command[-1]=='?' :
			# Register modetest 4 is read
			register_modetest = 4
			register = int(command[:-1])
			# Get details describing this register
			reply = self.read_register(register, 0, 4)

		elif len(command.split(','))==2:
			# Register modetest 6 is write
			register_modetest = 6
			# The first value is the register
			register = int(command.split(',')[0])
			# The second one is the command
			value = int(command.split(',')[1])

			self.write_register(register, value, 0, register_modetest)
			reply = self.read_register(register, 0, 4)
		else:
			reply = command+': command not defined'
		return (str(reply))
