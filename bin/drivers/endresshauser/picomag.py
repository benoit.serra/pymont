# -*- coding: utf-8 -*-
"""
Created on Thu Jan 07 10:40:00 2021

@author: bserra
"""
# IMPORTS
#########################
import requests
import sys
import collections
import requests 
from utils.converters import int_to_ieee_754

# CONSTANTS
#########################
# Code translations constants
PDIN_STRUCTURE = collections.OrderedDict()

PDIN_STRUCTURE.update(status={'pos':slice(28,30),
                              'comms':'',
                              'format':'08b'},
                      temperature={'pos':slice(24,28),
                                   'comms':' 1/10 Celsius',
                                   'format':'.2f'},
                      flow={'pos':slice(16,24),
                            'comms':'Liters per s',
                            'format':'.2f'},
                      totalizer={'pos':slice(8,16),
                                 'comms':'Liters',
                                 'format':'.2f'},
                      conductivity={'pos':slice(0,8),
                                    'comms':'microSiemens per centimeter',
                                    'format':'.2f'})


# METHODS 
#########################

#### Picomag ################################################################

class picomag(object):
    r"""Class with a basic driver for Picomag for pymont
    HTTP::134.171.5.200::1::INSTR
    """

    def __init__(self, 
                 RESOURCE_STRING, 
                 RESOURCE_MANAGER = None,
                 RESOURCE_ID = '',
                 RESOURCE_TIMEOUT = 200,
                 TERMINATION_STRING = '',
                 **kwargs):
        
        """Initialize internal variables and ethernet connection

        :param RESOURCE_STRING: The adress of the agilent
        :type RESOURCE_STRING: str
        :param TERMINATION_STRING: '\n' by default for agilent
        :type TERMINATION_STRING: str
        """
        try:
            self.instrument = iolinkdevice()
            self.instrument.IOlinkmaster = RESOURCE_STRING.split('::')[1]
            self.instrument.IOlinkdevice = RESOURCE_STRING.split('::')[2]
            self.instrument.api_endpoint = 'http://'+self.instrument.IOlinkmaster
            self.instrument.timeout = RESOURCE_TIMEOUT
            self.connected = True
            # I am using query by default, but query will not work on this case
            self.instrument.query = self._send_command
        except:
            self.connected = False

        try:
            self.inst_id = RESOURCE_ID+'_'
        except:
            self.inst_id = ''

    def _send_command(self,
                    command):
        """"""
        # Send command to ATH400M, the default identifier for the pump is #000
        # the ATH400M controller returns first the issued command, then a bit after you can read the response
        # hence why I use the while True loop to read until I get something that is not a VisaIOError
        if command == 'READ':
            data = """{ "code":"request",
                     "cid":1000,
                     "adr":"/iolinkmaster/port[[x]]/iolinkdevice/pdin/getdata"
                   }""".replace('[x]',self.instrument.IOlinkdevice)
                   
            # sending post request and saving response as response object 
            r = requests.post(url = self.instrument.api_endpoint, data = data) 

            # extracting data in json format 
            reply = r.json()
        else:
            reply = "IO link devices only support 'READ' command"

        return reply
        
    def dump_sensors(self):
        """"""
        results = dict()

        picomag_reply = self._send_command('READ')

        if picomag_reply['code'] == 200:
          data = picomag_reply['data']['value']
          #data_binary = format(int(data, 16), '0120b')
          for key in PDIN_STRUCTURE.keys():
              if key == 'status':
                  formatted_value = data[PDIN_STRUCTURE[key]['pos']]
              if key == 'temperature':
                  formatted_value = "{:.1f}".format(int(data[PDIN_STRUCTURE[key]['pos']], 16))
              else:
                  formatted_value = int_to_ieee_754(int(data[PDIN_STRUCTURE[key]['pos']], 16), exp_len=8, mant_len=23)
                      
              results.update({self.inst_id+key:[formatted_value, PDIN_STRUCTURE[key]['comms']]})

        elif picomag_reply['code'] == 400:
           results.update({})
        return results

class iolinkdevice(object):
    def __init__(self):
        self.test = 1

if __name__ == "__main__":
    rm = None
    
    ressource = 'HTTP::134.171.12.167::1::INSTR'
    
    connection = picomag(ressource, rm, 'WFM001')
    print (connection.connected)
    print (connection.dump_sensors())
    