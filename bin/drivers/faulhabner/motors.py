# -*- coding: utf-8 -*-
"""
Created on Wed May  2 17:45:05 2018

@author: bserra
"""

import socket
import json
import sys
import time
  ############
#### MCDC3002 ###################################################################
  ############
 
class MCDC3002(object):
    r"""Abstract class that implements the common driver for the model MDrive
    23 control drive. The driver implement the following x commands out 
    the x in the specification:

    """
    def __init__(self, 
                 RESOURCE_STRING, 
                 RESOURCE_MANAGER = None,
                 RESOURCE_ID = '',
                 RESOURCE_TIMEOUT = 5,
                 TERMINATION_STRING = '\r\n',
                 **kwargs):
        
        """Initialize internal variables and ethernet connection

        :param RESOURCE_STRING: The adress of the agilent
        :type RESOURCE_STRING: str
        :param TERMINATION_STRING: '\r\n' by default for schneider
        :type TERMINATION_STRING: str
        """
        try:
            self.instrument = RESOURCE_MANAGER.open_resource(RESOURCE_STRING,
                                                             open_timeout = RESOURCE_TIMEOUT)
            self.instrument.read_termination = TERMINATION_STRING
            self.instrument.write_termination = TERMINATION_STRING
            self.instrument.timeout = RESOURCE_TIMEOUT
            self.connected = True
        except:
            self.connected = False
            
        self.positions = []
        self._init_params()
        
        try:
            self.inst_id = RESOURCE_ID+'_'
            self._send_command = self.instrument.query
            self.identify()
            self.instrument.positions = kwargs['RESOURCE_MODEL'][self.instrument.identity]

        except:
            self.inst_id = ''
        
        if RESOURCE_MANAGER == None:
            sys.exit('No VISA resource manager found')
    
    def _init_params(self):
        """"""
        # Nothing to do now

    def identify(self):
        """Return identification of motor microcontroller"""
        microcontroller_type = self.instrument.query('GTYP')
        serial_number = self.instrument.query('GSER')
        self.instrument.identity = f'{microcontroller_type}_{serial_number}'
        
    def connect(self):
        """Start connection with schneider"""
        self.__init__()

    def __test_cmd(self,cmd):
        """Testing command raw output"""
        results = self.instrument.query(cmd)
        return results

    def _ack_cmd(self):
        """"""
        return None

    def dump_sensors(self):
        """"""
        
        results = dict()
        
        list_commands = [('POS', '[steps] Current actual position'),
                         ('TPOS', '[steps] Target position'),
                         ('GV', '[rpm] Current actual velocity'),
                         ('GRU', '[V] Current controller output value'),
                         ('GRC', '[mA] Current actual current in mA'),
                         ('TEM', '[C] Current housing temperature in Celsius')]
        
        for command, description in list_commands:
            reply = self._send_command(command)
            results.update({self.inst_id+command:[reply, description]})
        return results
