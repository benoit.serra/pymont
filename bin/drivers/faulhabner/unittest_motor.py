import pyvisa

rm = pyvisa.ResourceManager()
motor = rm.open_resource('ASRL/dev/ttyUSB5::INSTR')
motor.read_termination = '\r\n'

command = 'TEM'
reply = motor.query(command)
print (command, ':', reply)

motor.close()
rm.close()

print ('Done.')