## -*- coding: utf-8 -*-
#"""
#Created on Mon Feb 22 14:46:00 2021
#
#@author: bserra
#"""
#
#
import xlrd
import numpy as np
import pymodbus

from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.constants import Endian
from pymodbus.client.sync import ModbusTcpClient

import struct

import sys
import time

class Cryomech_CPAi(object):
	r"""Class that implements the common driver for the TeePee 
	(Imago500) measurement and control unit.
	This instrument is controlled through the MODBUS interface (RS422)
	so we can't use pyvisa 
	"""
	def __init__(self,
				RESOURCE_STRING, 
				RESOURCE_MANAGER = None,
				RESOURCE_ID = '',
				RESOURCE_TIMEOUT = 5,
				TERMINATION_STRING = chr(13)+chr(10),
				**kwargs):		 
		"""Initialize internal variables and ethernet connection

		:param RESOURCE_STRING: The adress of the Lakeshore
		:type RESOURCE_STRING: str
		:param TERMINATION_STRING: '\r\n' by default for Pfeiffer gauges
		:type TERMINATION_STRING: str
		"""

		comm_infos = RESOURCE_STRING.split('::')
		try:
			self.instrument = ModbusTcpClient(comm_infos[1])		
			self.instrument.query = self._send_command				
			self.connected = True
			self.startstop = {'START':self.start,
						 'STOP':self.stop}
		except:
			self.connected = False

		try:
			self.inst_id = RESOURCE_ID+'_'
		except:
			self.inst_id = ''
	
	def _send_command(self, 
					  command):
		""""""
		if command in self.startstop:
			response = self.startstop[command]()
			reply = response
		elif '?' in command:
			register = int(command[:-1])
			reply = self.instrument.read_input_registers(register, 1)
			reply = reply.registers
		else:
			reply = 'Not a query'
		return reply  
  
	def identify(self):
		""""""
		reply = self._send_command(0x1F, 3)
		return reply.registers
	
	def start(self):
		self.instrument.write_register(1, 1)
		reply = self.instrument.read_holding_registers(1, 1)
		if reply.registers[0] == 1:
			response = "Compressor started"
		else:
			response = f"Sent start register 40001: 0x0001 (1), but status is {reply.register}"

	def stop(self):
		self.instrument.write_register(1, 255)
		reply = self.instrument.read_holding_registers(1, 1)
		if reply.registers[0] == 255:
			response = "Compressor stopped"
		else:
			response = f"Sent start register 40001: 0x00FF (255), but status is {reply.register}"
  
	def _convert_register(self, regPair):
		# Reconstruct the result by order the four bytes as described in the documentation
		result_swap = b''.join(struct.pack(Endian.Little + 'H', x) for x in regPair)
		# Unpack it into a float
		value = struct.unpack('f', result_swap)[0]
		# Update the dictionnary
		#comment = name_float32_registers[idx]
		return value

	def dump_sensors(self):
		""""""
		results = dict()
		reply = self.instrument.read_input_registers(0x22, 18)
		register_listvalues = reply.registers
		# Some description of the register
		name_float32_registers = ['Detected RPM in 1/100th Scale (int)',
								  'Software Variant (int)',
								  'Inverter Frequency in 1/10th Hz (int)',
								  'Inverter Current in 1/10th Amps (int)',
								  '_','_',
								  'Coolant In Temp in 1/10th Scale (int)',
								  'Coolant Out Temp in 1/10th Scale (int)',
								  'Oil Temp in 1/10th Scale (int)',
								  'Helium Temp in 1/10th Scale (int)',
								  'Low Pressure in 1/10th Scale (int)',
								  'Low Pressure Average in 1/10th Scale (int)',
								  'High Pressure in 1/10th Scale (int)',
								  'High Pressure Average in 1/10th Scale (int)',
								  'Delta Pressure Average in 1/10th Scale (int)',
								  'Motor Current in 1/10th Scale (int)',
								  'Hours Of Operation in 1/10th Scale (int32)']
		# Creating keywords from the name using capital letters
		registers_keyw = list()
		for name in name_float32_registers:
			if name != '_':
				nametag = ''.join([c for c in name if c.isupper()])
				registers_keyw.append(nametag)
			else:
				registers_keyw.append('_')
		print (registers_keyw)
		# Grouping all float32 registers by pair of two
		registers = list()
		for idx, reg in enumerate(register_listvalues[:-1]):
			if idx < 16:
				registers.append(reg)
			else:
				registers.append([register_listvalues[idx], register_listvalues[idx+1]])
			
		# For each pair of value
		for idx, value_register in enumerate(registers):
			if isinstance(value_register, list):
				# Hours of operation is just a weird list
				value = value_register[0]/10
				results.update({self.inst_id+registers_keyw[idx]:(value, comment)})
			elif name_float32_registers[idx] != '_':
				value = value_register / 10
				# Update the dictionnary
				comment = name_float32_registers[idx]
				results.update({self.inst_id+registers_keyw[idx]:(value, comment)})
			
		#for keyw in self.JUMO_registers.keys():
		#	result = self._send_command(self.JUMO_registers[keyw], None, DICT_IMAGO500)
		#	results.update({self.inst_id+keyw:(str(result[0]),'[AU] '+result[1][0])})
		return results


class Cryomech_CPA(object):
	r"""Class that implements the common driver for the TeePee 
	(Imago500) measurement and control unit.
	This instrument is controlled through the MODBUS interface (RS422)
	so we can't use pyvisa 
	"""
	def __init__(self,
				RESOURCE_STRING, 
				RESOURCE_MANAGER = None,
				RESOURCE_ID = '',
				RESOURCE_TIMEOUT = 5,
				TERMINATION_STRING = chr(13)+chr(10),
				**kwargs):		 
		"""Initialize internal variables and ethernet connection

		:param RESOURCE_STRING: The adress of the Lakeshore
		:type RESOURCE_STRING: str
		:param TERMINATION_STRING: '\r\n' by default for Pfeiffer gauges
		:type TERMINATION_STRING: str
		"""

		comm_infos = RESOURCE_STRING.split('::')
		try:
			self.instrument = ModbusTcpClient(comm_infos[1])	
			#self.instrument = ModbusClient(method='rtu', port=comm_infos[1], timeout=1, baudrate=115200)	
			self.instrument.query = self._send_command				
			self.connected = True
			self.startstop = {'START':self.start,
						 'STOP':self.stop}
		except:
			self.connected = False

		try:
			self.inst_id = RESOURCE_ID+'_'
		except:
			self.inst_id = ''
	
	def _send_command(self, 
					  command):
		""""""
		if command in self.startstop:
			response = self.startstop[command]()
			reply = response
		elif '?' in command:
			register = int(command[:-1])
			reply = self.instrument.read_input_registers(register, 2)
			reply = reply.registers
		else:
			reply = 'Not a query'
		return reply  
  
	def identify(self):
		""""""
		reply = self._send_command(0x1F, 2)
		return reply.registers
	
	def start(self):
		self.instrument.write_register(1, 1)
		reply = self.instrument.read_holding_registers(1, 1)
		if reply.registers[0] == 1:
			response = "Compressor started"
		else:
			response = f"Sent start register 40001: 0x0001 (1), but status is {reply.register}"

	def stop(self):
		self.instrument.write_register(1, 255)
		reply = self.instrument.read_holding_registers(1, 1)
		if reply.registers[0] == 255:
			response = "Compressor stopped"
		else:
			response = f"Sent start register 40001: 0x00FF (255), but status is {reply.register}"
  
	def _convert_register(self, regPair):
		# Reconstruct the result by order the four bytes as described in the documentation
		result_swap = b''.join(struct.pack(Endian.Little + 'H', x) for x in regPair)
		# Unpack it into a float
		value = struct.unpack('f', result_swap)[0]
		# Update the dictionnary
		#comment = name_float32_registers[idx]
		return value

	def dump_sensors(self):
		""""""
		results = dict()
		reply = self.instrument.read_input_registers(0x01, 33)
		register_listvalues = reply.registers
		# Some description of the register
		name_float32_registers = ['Coolant In Temp',
								  'Coolant Out Temp',
								  'Oil Temp',
								  'Helium Temp',
								  'Low Pressure',
								  'Low Pressure Average',
								  'High Pressure',
								  'High Pressure Average',
								  'Delta Pressure Average',
								  'Motor Current',
								  'Hours Of Operation']
		# Creating keywords from the name using capital letters								 
		registers_keyw = ''.join([c for c in name_float32_registers if c.isupper()])
		
		# Grouping all float32 registers by pair of two
		float32_registers = [[register_listvalues[idx], register_listvalues[idx+1]] for idx, reg in enumerate(register_listvalues) if (idx>=6) & (idx<28) & (idx%2==0)]
		
		# For each pair of value
		for idx, float_register in enumerate(float32_registers):
				# Reconstruct the result by order the four bytes as described in the documentation
				result_swap = b''.join(struct.pack(Endian.Little + 'H', x) for x in float_register)
				# Unpack it into a float
				value = struct.unpack('f', result_swap)[0]
				# Update the dictionnary
				comment = name_float32_registers[idx]
				results.update({self.inst_id+name_float32_registers[idx]:(value, comment)})
				
		#for keyw in self.JUMO_registers.keys():
		#	result = self._send_command(self.JUMO_registers[keyw], None, DICT_IMAGO500)
		#	results.update({self.inst_id+keyw:(str(result[0]),'[AU] '+result[1][0])})
		return results

class CPA289C(Cryomech_CPA):
	r"""Class inheriting from minimalmodbus.Instrument to add close and query methods needed for
	pymont.
	"""

	def __init__(self, **kwargs):
		super(CPA289C, self).__init__(**kwargs)

	def close(self):
		r"""Method to close the communication with the JUMO using the 
		CLOSE methods defined in pymont
		"""
		self.serial.close()

class CPA286I(Cryomech_CPAi):
	r"""Class inheriting from minimalmodbus.Instrument to add close and query methods needed for
	pymont.
	"""

	def __init__(self, **kwargs):
		super(CPA286I, self).__init__(**kwargs)

	def close(self):
		r"""Method to close the communication with the JUMO using the 
		CLOSE methods defined in pymont
		"""
		self.serial.close()


if __name__ == '__main__':
	connection = CPA289C(RESOURCE_STRING='MODBUSTCP::134.171.36.37',
                       RESOURCE_ID='CC001')
	#value = connection._send_command('40?')
	#print (value)
	#print (connection._send_command('7?'))
	values = connection.dump_sensors()
	print (values)
	# Start/stop test
	"""
	connection.instrument.query('START')
	time.sleep(1)
	connection.instrument.query('STOP')
	"""


	#connection = CPA289C(RESOURCE_STRING='MODBUSRTU::/dev/ttyUSB1',
    #                   RESOURCE_ID='CC001')
	#print (connection.connected)
	#value = connection._send_command('40?')
	#print (value)
	#print (connection._send_command('7?'))
	#values = connection.dump_sensors()
	#print (values)
	# Start/stop test


