# -*- coding: utf-8 -*-
"""
Created on Mon Jan 28 12:19:21 2019

@author: bserra

The T395 can communicate through serial.
Read end is \x03 (chr(3))
Asking for data is the A character.
Temperatures are at the 8, 10, 12 and 14 position of the answer's bytes string
"""
# IMPORTS
#########################
import json
import sys

import time

# METHODS 
#########################

#### PCE T395 ################################################################

class T395(object):
    r"""Abstract class that implements the common driver for the SR80 blackbody
    from CI systems
    """

    def __init__(self, 
                 RESOURCE_STRING, 
                 RESOURCE_MANAGER = None,
                 RESOURCE_ID = '',
                 RESOURCE_TIMEOUT = 100,
                 TERMINATION_STRING = chr(3),
                 **kwargs):
        
        """Initialize internal variables and ethernet connection

        :param RESOURCE_STRING: The adress of the agilent
        :type RESOURCE_STRING: str
        :param TERMINATION_STRING: '\n' by default for agilent
        :type TERMINATION_STRING: str
        """
        try:
            self.instrument = RESOURCE_MANAGER.open_resource(RESOURCE_STRING,
                                                             open_timeout = RESOURCE_TIMEOUT)
            self.instrument.read_termination = TERMINATION_STRING
            self.instrument.write_termination = ''
            self.instrument.timeout = RESOURCE_TIMEOUT
            self.instrument.query = self._send_command
            self.instrument.write('K')
            self.connected = True
        except:
            self.connected = False

        try:
            self.inst_id = RESOURCE_ID+'_'
        except:
            self.inst_id = ''
        
        if RESOURCE_MANAGER == None:
            sys.exit('No VISA resource manager found')

    def _send_command(self,
                    command):
        """"""
        if command not in ['A']:
            reply = 'Thermometer support only the A command'
        else:
            self.instrument.write(command)
            # If sleep for one second only, I get random timeouts
            time.sleep(1.5)
            reply = self.instrument.read_raw()
            print (reply)
        return reply

    def fetch(self):
        """Send a command A and parse it into a dictionnary
r
        :returns: the dictionnary of the measurements for all 4 channels
        :rtype: dict
        """   
        results = dict()
        data = self._send_command('A')
        temperatures_index = [8, 10, 12, 14]
        # Data[6] indicates the position of the floating point, usually 1
        # I hardcoded the temperature by dividing by 10 instead of placing
        # the '.' in respect to data[6] value, might want to do it later.
        #print (data[6])
        floatingpoint_position = []
        for idx, temp in enumerate(temperatures_index):
            results.update({self.inst_id+f'TEMP{idx}':[data[temp]/10, '[Celsius]']})

        return results
        
    def dump_sensors(self):
        """"""
        results = {**self.fetch()}
        return results

if __name__ == '__main__':
    import pyvisa
    
    ResourceManager = pyvisa.ResourceManager()
    
    ressource_name = ''
    
    thermo = T395('ASRL/dev/ttyUSB1::INSTR',
                  ResourceManager,
                  'THERM001')
                    
    dict_resp = thermo.dump_sensors()
    print (dict_resp)
