# -*- coding: utf-8 -*-
"""
Created on Wed Mar  3 15:42:00 2021

@author: bserra
"""

class DR69931(object):
    r"""Abstract class that implements the common driver for the model DP831A
    Programmable DC power supply. The driver implement the following x commands

    """

    def __init__(self, 
                 RESOURCE_STRING, 
                 RESOURCE_MANAGER = None,
                 RESOURCE_ID = '',
                 RESOURCE_TIMEOUT = 5,
                 TERMINATION_STRING = '\r',
                 **kwargs):
        
        """Initialize internal variables and ethernet connection

        :param RESOURCE_STRING: The adress of the Rigol device
        :type RESOURCE_STRING: str
        :param TERMINATION_STRING: '\n' by default for Rigol
        :type TERMINATION_STRING: str
        """
        try:
            self.instrument = RESOURCE_MANAGER.open_resource(RESOURCE_STRING,
                                                             open_timeout = RESOURCE_TIMEOUT)
            self.instrument.read_termination = TERMINATION_STRING
            self.instrument.write_termination = TERMINATION_STRING
            self.instrument.timeout = RESOURCE_TIMEOUT
            self.connected = True
        except:
            self.connected = False

        try:
            self.inst_id = RESOURCE_ID+'_'
        except:
            self.inst_id = ''
        
        if RESOURCE_MANAGER == None:
            sys.exit('No VISA resource manager found')

    def identify(self):
        return self.instrument.query('IDN?') 
       
    def get_displayed_values(self):
        list_values = []
        for command, desc in [('AMPS?', 'Amps as displayed on front panel'),
                              ('VOLTS?', 'Voltage as displayed on front panel'),
                              ('WATTS?', 'Power as displayed on front panel'),]:
            reply = self.instrument.query(command)
            list_values.append((command[:-2], reply, desc))
        return list_values
        
     
    def fetch(self):
        """Send a command and check if it is positively acknowledged

        :param command: The command to send
        :type command: str
        :raises IOError: if the negative acknowledged or a unknown response
            is returned
        """   
        results = dict()
        for var, value, desc in self.get_displayed_values():
            results.update({self.inst_id+var:[value, desc]})
        return results
        
    def dump_sensors(self):
        """"""
        results = {**self.fetch()}

        return results