# -*- coding: utf-8 -*-
"""
Created on Mon Jan 28 12:19:21 2019

@author: bserra
"""
# IMPORTS
#########################
import json
import sys
import visa

import time

# METHODS 
#########################

#### SR80 ################################################################

class SR80(object):
    r"""Abstract class that implements the common driver for the SR80 blackbody controller
    from CI systems
    """

    def __init__(self, 
                 RESOURCE_STRING, 
                 RESOURCE_MANAGER = None,
                 RESOURCE_ID = '',
                 RESOURCE_TIMEOUT = 5,
                 TERMINATION_STRING = '\r\n',
                 **kwargs):
        
        """Initialize internal variables and ethernet connection

        :param RESOURCE_STRING: The adress of the agilent
        :type RESOURCE_STRING: str
        :param TERMINATION_STRING: '\n' by default for agilent
        :type TERMINATION_STRING: str
        """
        try:
            self.instrument = RESOURCE_MANAGER.open_resource(RESOURCE_STRING,
                                                             open_timeout = RESOURCE_TIMEOUT)
            self.instrument.read_termination = TERMINATION_STRING
            self.instrument.write_termination = TERMINATION_STRING
            self.instrument.timeout = RESOURCE_TIMEOUT
            self.connected = True
            # This instrument does not have a specific formatting
            # so using the norman instrumetn.query to send instructions
            #self.instrument.query = self._send_command
        except:
            self.connected = False

        try:
            self.inst_id = RESOURCE_ID+'_'
        except:
            self.inst_id = ''
        
        if RESOURCE_MANAGER == None:
            sys.exit('No VISA resource manager found')
        
    def fetch(self):
        """Send a command :MEAS:RES and parse it into a dictionnary
r
        :returns: the dictionnary of the measurements for all 4 channels
        :rtype: dict
        """   
        results = dict()
        for command in ['RT', 'RD', 'RG', 'RM', 'RR', 'RV']:            
            response = self.instrument.query(command)
            print (response)
            results.update({self.inst_id+command.upper():[response, '[unit]']})
        
        return results
        
    def dump_sensors(self):
        """"""
        results = {**self.fetch()}
        return results

class SR20(object):
    r"""Abstract class that implements the common driver for the SR80 blackbody controller
    from CI systems
    """

    def __init__(self, 
                 RESOURCE_STRING, 
                 RESOURCE_MANAGER = None,
                 RESOURCE_ID = '',
                 RESOURCE_TIMEOUT = 5,
                 TERMINATION_STRING = '',
                 **kwargs):
        
        """Initialize internal variables and ethernet connection

        :param RESOURCE_STRING: The adress of the agilent
        :type RESOURCE_STRING: str
        :param TERMINATION_STRING: '\n' by default for agilent
        :type TERMINATION_STRING: str
        """
        try:
            self.instrument = RESOURCE_MANAGER.open_resource(RESOURCE_STRING,
                                                             open_timeout = RESOURCE_TIMEOUT)
            self.instrument.read_termination = TERMINATION_STRING
            self.instrument.write_termination = TERMINATION_STRING
            self.instrument.timeout = RESOURCE_TIMEOUT
            self.connected = True
            # This instrument does not have a specific formatting
            # so using the norman instrumetn.query to send instructions
            #self.instrument.query = self._send_command
        except:
            self.connected = False

        try:
            self.inst_id = RESOURCE_ID+'_'
        except:
            self.inst_id = ''
        
        if RESOURCE_MANAGER == None:
            sys.exit('No VISA resource manager found')
            
    def status(self):
        """Read serial poll to check on status"""
        
    def identify(self):
        """Send RV to controller to get the information on the version"""
        response = self.instrument.query('RV')
        return response            

    def status(self):
        """Send Serial Poll for status register"""
        response = self.instrument.stb
        return {f'{self.inst_id}STB': [bin(response), 'Status [binary]']}

    def fetch(self):
        """Send a command :MEAS:RES and parse it into a dictionnary
        :returns: the dictionnary of the measurements for all 4 channels
        :rtype: dict
        """   
        results = dict()
        commands = ['RT', 'RA', 'RB', 'RF', 'RW', 'RP']
        description = ['Blackbody temperature [C]',
                       'Aperture wheel position [nounit]',
                       'Filter wheel position [nounit]',
                       'Chopper frequency [Hz]',
                       'Number of chopper blades [nounit]',
                       'Chopper phase [deg]']
        for command, desc in zip(commands, description):
            response = self.instrument.query(command)
            results.update({self.inst_id+command.upper():[float(response), desc]})
        results.update({**self.status()})
        return results

    def dump_sensors(self):
        """"""
        results = {**self.fetch()}
        return results
        
#### SR800R ################################################################

class SR800R(object):
    r"""Abstract class that implements the common driver for the SR80 blackbody controller
    from CI systems
    """

    def __init__(self, 
                 RESOURCE_STRING, 
                 RESOURCE_MANAGER = None,
                 RESOURCE_ID = '',
                 RESOURCE_TIMEOUT = 100,
                 TERMINATION_STRING = '\r\n',
                 **kwargs):
        
        """Initialize internal variables and ethernet connection

        :param RESOURCE_STRING: The adress of the agilent
        :type RESOURCE_STRING: str
        :param TERMINATION_STRING: '\n' by default for agilent
        :type TERMINATION_STRING: str
        """
        try:
            self.instrument = RESOURCE_MANAGER.open_resource(RESOURCE_STRING,
                                                             open_timeout = RESOURCE_TIMEOUT)
            self.instrument.read_termination = TERMINATION_STRING
            self.instrument.write_termination = TERMINATION_STRING
            self.instrument.timeout = RESOURCE_TIMEOUT
            self.connected = True
            # If the connection is serial, the setting in the blackbody for the baud rate is 115200.
            # inferior baud rate will result in timeouts
            if 'ASRL' in RESOURCE_STRING:
              self.instrument.baud_rate = 115200
            print (self.instrument.query('GETTARGETTEMPERATURE'))
            # This instrument does not have a specific formatting
            # so using the norman instrumetn.query to send instructions
            #self.instrument.query = self._send_command
        except:
            self.connected = False

        try:
            self.inst_id = RESOURCE_ID+'_'
        except:
            self.inst_id = ''
        
        print (self.connected)
        if RESOURCE_MANAGER == None:
            sys.exit('No VISA resource manager found')
        
    def fetch(self):
        """Send a command :MEAS:RES and parse it into a dictionnary
r
        :returns: the dictionnary of the measurements for all 4 channels
        :rtype: dict
        """   
        results = dict()
        for (command, unit) in [('GETTEMPERATURE', '[Celsius]'), ('GETTARGETTEMPERATURE', '[Celsius]'), ('ISTEMPERATURESTABLE', '[Boolean]')]:            
            response = self.instrument.query(command)
            print (response)
            results.update({self.inst_id+command.upper():[response, unit]})
        
        return results

    def identify(self):
        """Send RV to controller to get the information on the version"""
        response = self.instrument.query('GETSYSTEMINFO')
        return response  
        
    def dump_sensors(self):
        """"""
        results = {**self.fetch()}
        return results
        
if __name__ == "__main__":
    # Test SR20
    
    #rm = visa.ResourceManager('@py')
    #interface = 'GPIB0::15'
    #blackbody_controller = SR20(interface,
    #                            rm)
    #print (blackbody_controller.identify())
    #print (blackbody_controller.dump_sensors())

    rm = visa.ResourceManager('@py')
    interface = 'ASRL/dev/ttyUSB0::INSTR'
    blackbody_controller = SR800R(interface, rm)
    print (blackbody_controller.identify())
    print (blackbody_controller.dump_sensors())