# -*- coding: utf-8 -*-
"""
Created on Mon Jan 28 12:19:21 2019

@author: bserra
"""
# IMPORTS
#########################
import json
import sys

import time

# METHODS 
#########################

#### DSO 5034A ################################################################

class VED24(object):
    r"""Abstract class that implements the common driver for the SR80 blackbody
    from CI systems
    """

    def __init__(self, 
                 RESOURCE_STRING, 
                 RESOURCE_MANAGER = None,
                 RESOURCE_ID = '',
                 RESOURCE_TIMEOUT = 5,
                 TERMINATION_STRING = '\r',
                 **kwargs):
        
        """Initialize internal variables and ethernet connection

        :param RESOURCE_STRING: The adress of the agilent
        :type RESOURCE_STRING: str
        :param TERMINATION_STRING: '\n' by default for agilent
        :type TERMINATION_STRING: str
        """
        try:
            self.instrument = RESOURCE_MANAGER.open_resource(RESOURCE_STRING,
                                                             open_timeout = RESOURCE_TIMEOUT)
            self.instrument.read_termination = TERMINATION_STRING
            self.instrument.write_termination = TERMINATION_STRING
            self.instrument.timeout = RESOURCE_TIMEOUT
            self.instrument.status = 'Unknown'
            self.instrument.query = self._send_command
            self.connected = True
        except:
            self.connected = False

        try:
            self.inst_id = RESOURCE_ID+'_'
        except:
            self.inst_id = ''
        
        if RESOURCE_MANAGER == None:
            sys.exit('No VISA resource manager found')

    def _send_command(self,
                    command):
        """"""
        if command not in ['A', '@']:
            reply = 'Shutter support only @ (open) and A (close) commands'
        else:
            status_dict = {'A': 'Close',
                           '@': 'Open'}
            self.instrument.status = status_dict[command]
            self.instrument.write(command)
            reply = 'Shutter '+status_dict[command]
        return reply

    def fetch(self):
        """Send a command :MEAS:RES and parse it into a dictionnary
r
        :returns: the dictionnary of the measurements for all 4 channels
        :rtype: dict
        """   
        results = dict()
        
        results.update({self.inst_id+'STATUS':[self.instrument.status, '[no unit]']})

        return results
        
    def dump_sensors(self):
        """"""
        results = {**self.fetch()}
        return results

if __name__ == '__main__':
    import pyvisa
    
    ResourceManager = pyvisa.ResourceManager()
    
    ressource_name = ''
    
    shutter = VED24('ASRL/dev/ttyUSB3::INSTR',
                    ResourceManager,
                    'SHUT001')
                    
    resp = shutter._send_command('@')
    print (resp, shutter.instrument.status)
