## -*- coding: utf-8 -*-
#"""
#Created on Mon Mar 11 14:52:54 2019
#
#@author: bserra
#"""
#
#
import xlrd
import numpy as np
import minimalmodbus
import sys

# Code translations constants


class ECN100(object):
    r"""Class that implements the common driver for the Extended blackbody ECN100
    This instrument can be controlled through GPIB/Ethernet and Serial interface
    """
    def __init__(self,
                RESOURCE_STRING,
                RESOURCE_MANAGER = None,
                RESOURCE_ID = '',
                RESOURCE_TIMEOUT = 5,
                TERMINATION_STRING = chr(13),
                **kwargs):
        """Initialize internal variables and serial connection

        :param RESOURCE_STRING: The adress of the Blackbody
        :type RESOURCE_STRING: str
        :param TERMINATION_STRING: '\r' by default for ECN/DCN models
        :type TERMINATION_STRING: str
        """

        comm_infos = RESOURCE_STRING.split('::')
        try:
            self.instrument = RESOURCE_MANAGER.open_resource(RESOURCE_STRING,
                                                             open_timeout = RESOURCE_TIMEOUT)
            self.instrument.timeout  = RESOURCE_TIMEOUT   # seconds
            self.instrument.read_termination = TERMINATION_STRING   # string
            self.instrument.write_termination = TERMINATION_STRING   # string
            self.instrument.timeout  = RESOURCE_TIMEOUT   # seconds
            self.connected = True
            self.instrument.query = self.query
        except:
            self.connected = False

        try:
            self.inst_id = RESOURCE_ID+'_'
        except:
            self.inst_id = ''

        self.ECN_functions = kwargs['RESOURCE_CONFIG']
        if RESOURCE_MANAGER == None:
            sys.exit('No VISA resource manager found')

    def _format_command(self, function_number, mode, parameter=None):
        # chr(mode) returns b'\x00', str(mode) returns b'0'. Apparently
        # the hexa notation doesn't work (Command Error!)
        if parameter is None:
            formatting = [chr(27), str(mode), chr(function_number), chr(13)]
            # after encoding to utf-8 you need to take only last byte
            # ex: chr(136).encode('utf-8') = b'\xc2\x88'
            #     it is \x88 that we want
            fmt = [chr(27).encode('utf-8'),
                   str(mode).encode('utf-8'),
                   chr(function_number).encode('utf-8')[-1:],
                   chr(13).encode('utf-8')]        
        else:
            #fmt = [char.encode('utf-8') for char in formatting]
            # parameter format is sign+6digit, the two last are the float part
            # the four first the integer
            parameter = '{:+07d}'.format(int('{:.2f}'.format(parameter).replace('.','')))
            # parameter = '{0:+}'.format(parameter)
            print(function_number, ':', parameter)
            formatting = [chr(27), str(mode), chr(function_number), parameter, chr(13)]
            # after encoding to utf-8 you need to take only last byte
            # ex: chr(136).encode('utf-8') = b'\xc2\x88'
            #     it is \x88 that we want
            fmt = [chr(27).encode('utf-8'),
                   str(mode).encode('utf-8'),
                   chr(function_number).encode('utf-8')[-1:],
                   parameter.encode('utf-8'),
                   chr(13).encode('utf-8')]
            #fmt = [char.encode('utf-8') for char in formatting]
        return b''.join(fmt) 

    def _send_command(self,
                    command):
        """"""
        # If blackbody reach stability it will send '1' that I don't use 
        # this read should get rid of it
        try:
            ack = self.instrument.read_raw()
        except:
            pass
        self.instrument.write_raw(command)
        reply = self.instrument.read_raw()
        return self._strip_resp(reply)

    def query(self, command):
        # If blackbody reach stability it will send '1' that I don't use 
        # this read should get rid of it
        try:
            ack = self.instrument.read_raw()
        except:
            pass
        if command[-1]=='?':
            function_number = int(command[:-1])
            formatted_command = self._format_command(function_number, 0)
            reply = self._send_command(formatted_command)
        else:
            function_number, parameter = command.split(',')
            # parameter is a float
            formatted_command = self._format_command(int(function_number), 1, parameter=float(parameter))
            reply = self._send_command(formatted_command)
        return reply
        
    def _strip_resp(self, bytes_array):
        string = bytes_array.decode('cp437')
        return string

    def dump_sensors(self):
        """"""
        results = dict()
        list_commands = [(self._format_command(65, 0), 'BB-SETP', 'Setpoint [C]'),
                         (self._format_command(82, 0), 'BB-TEMP', 'Temperature [C]'),
                         (self._format_command(84, 0), 'AMB-TEMP', 'Ambiant temperature [C]'),
                         (self._format_command(92, 0), 'STAB', 'Stability flag [bool]')]

        for command, keyw, comment in list_commands:
            result = self._send_command(command)
            # ack = self.instrument.read_raw()
            results.update({self.inst_id+keyw:(str(result[2:-1]),comment)})
        return results



if __name__ == '__main__':
    connection = imago500('/dev/ttyUSB1','randomresourcemanager','T001')
    connection.dump_sensors()
    connection.instrument.serial.close()



