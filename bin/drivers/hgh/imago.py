## -*- coding: utf-8 -*-
#"""
#Created on Mon Mar 11 14:52:54 2019
#
#@author: bserra
#"""
#
#
import xlrd
import numpy as np
import minimalmodbus
import sys

# Code translations constants


class ECN100(object):
    r"""Class that implements the common driver for the Extended blackbody ECN100
    This instrument can be controlled through GPIB/Ethernet and Serial interface
    """
    def __init__(self,
                RESOURCE_STRING,
                RESOURCE_MANAGER = None,
                RESOURCE_ID = '',
                RESOURCE_TIMEOUT = 5,
                TERMINATION_STRING = '\r',
                **kwargs):
        """Initialize internal variables and serial connection

        :param RESOURCE_STRING: The adress of the Blackbody
        :type RESOURCE_STRING: str
        :param TERMINATION_STRING: '\r' by default for ECN/DCN models
        :type TERMINATION_STRING: str
        """
        # Important for enabling disconnection/connection.
        # if this is not here, the minimalmodbus library will display
        # ' Attempting to use a port that is not open' after reconnect
        minimalmodbus.CLOSE_PORT_AFTER_EACH_CALL = True

        comm_infos = RESOURCE_STRING.split('::')
        try:
            self.instrument = RESOURCE_MANAGER.open_resource(RESOURCE_STRING,
                                                             open_timeout = RESOURCE_TIMEOUT)
            self.instrument.timeout  = RESOURCE_TIMEOUT   # seconds
            self.connected = True
        except:
            self.connected = False

        try:
            self.inst_id = RESOURCE_ID+'_'
        except:
            self.inst_id = ''

        self.ECN_functions = kwargs['RESOURCE_CONFIG']
        if RESOURCE_MANAGER == None:
            sys.exit('No VISA resource manager found')

    def _send_command(self,
                    command):
        """"""
        reply = self.instrument.query(command)
        return reply

    def dump_sensors(self):
        """"""
        results = dict()
        list_commands = [(b'\x1b0\x41\r', 'BB-SETP', 'Setpoint [C]'),
                         (b'\x1b0\x52\r', 'BB-TEMP', 'Temperature [C]'),
                         (b'\x1b0\x54\r', 'AMB-TEMP', 'Ambiant temperature [C]'),
                         (b'\x1b0\x5c\r', 'STAB', 'Stability flag [bool]')]


        for command, keyw, comment in list_commands:
            result = self._send_command(command)
            results.update({self.inst_id+keyw:(str(result[0]),comment)})
        return results



if __name__ == '__main__':
    connection = imago500('/dev/ttyUSB1','randomresourcemanager','T001')
    connection.dump_sensors()
    connection.instrument.serial.close()



