# -*- coding: utf-8 -*-
"""
Created on Mon Jan 28 12:19:21 2020

@author: bserra
"""
# IMPORTS
#########################
import pyvisa
from pyvisa import VisaIOError
import sys
#import time

# METHODS 
#########################

#### ATH400M ################################################################

class ATH400M(object):
    r"""Class with a basic driver for ATH400M for pymont
    """

    def __init__(self, 
                 RESOURCE_STRING, 
                 RESOURCE_MANAGER = None,
                 RESOURCE_ID = '',
                 RESOURCE_TIMEOUT = 200,
                 TERMINATION_STRING = '\r',
                 **kwargs):
        
        """Initialize internal variables and ethernet connection

        :param RESOURCE_STRING: The adress of the agilent
        :type RESOURCE_STRING: str
        :param TERMINATION_STRING: '\n' by default for agilent
        :type TERMINATION_STRING: str
        """
        try:
            self.instrument = RESOURCE_MANAGER.open_resource(RESOURCE_STRING,
                                                             open_timeout = RESOURCE_TIMEOUT)
            self.instrument.read_termination = TERMINATION_STRING
            self.instrument.write_termination = TERMINATION_STRING
            self.instrument.timeout = RESOURCE_TIMEOUT
            self.connected = True
            # I am using query by default, but query will not work on this case
            self.instrument.query = self._send_command
        except:
            self.connected = False

        try:
            self.inst_id = RESOURCE_ID+'_'
        except:
            self.inst_id = ''
        
        if RESOURCE_MANAGER == None:
            sys.exit('No VISA resource manager found')

    def _send_command(self,
                    command):
        """"""
        # Send command to ATH400M, the default identifier for the pump is #000
        # the ATH400M controller returns first the issued command, then a bit after you can read the response
        # hence why I use the while True loop to read until I get something that is not a VisaIOError
        reply = list()
        self.instrument.write('#000'+command)
        test = self.instrument.read()
        while True:
            try:
                reply.append(self.instrument.read_raw())
            except VisaIOError:
                break
        return str(reply)
        
    def dump_sensors(self):
        """"""
        results = dict()
        # 2 is RPM, 9 is current (mA), 11 pump temp1, 13 controller temp
        list_parameters = [(2, 'RPM', 'Turbo molecular pump rotation per minute [rpm]'),
                           (9, 'CURRENT', 'Drawn current [mA]'),
                           (11, 'PUMP-TEMP', 'Pump body temperature [C]'),
                           (13, 'CONT-TEMP', 'Controller temperature [C]')]
        # Returns the status of the internal dynamical paramaters
        reply = self._send_command('STA')   
        result = reply.split(',')               
        for index, keyw, comment in list_parameters:
            results.update({self.inst_id+keyw:(str(result[index]),comment)})
        return results


if __name__ == "__main__":
    rm = pyvisa.ResourceManager()
    
    ressource = 'ASRL/dev/ttyUSB3::INSTR'
    
    connection = ATH400M(ressource, rm, 'TMP001')
    print (connection.dump_sensors())
    connection.instrument.close()
    
    rm.close()