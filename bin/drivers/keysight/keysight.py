# -*- coding: utf-8 -*-
"""
Created on Mon  Feb  8 13:00:00 2021

@author: bserra
"""

import json
import time
import sys

#### 33600A #################################################################

class WG33600A(object):
    r"""Abstract class that implements the common driver for the model DG1000Z
    Programmable waveform generator. The driver implement the following x commands

    * *IDN?: Identifiation query (model identification)

    This class also contains the following class variables, for the specific
    characters that are used in the communication:

    :var LF: Line feed, chr(10), \\n
    """

    LF = chr(10)

    def __init__(self, 
                 RESOURCE_STRING, 
                 TERMINATION_STRING = '\r\n',
                 RESOURCE_ID = '',
                 RESOURCE_MANAGER = None,
                 RESOURCE_TIMEOUT = 5,
                 **kwargs):
        """Initialize internal variables and ethernet connection

        :param RESOURCE_STRING: The adress of the Lakeshore
        :type RESOURCE_STRING: str
        :param TERMINATION_STRING: '\n' by default for Rigol devices
        :type TERMINATION_STRING: str
        """
        try:
            self.instrument = RESOURCE_MANAGER.open_resource(RESOURCE_STRING,
                                                             open_timeout = RESOURCE_TIMEOUT)
            self.instrument.read_termination = TERMINATION_STRING
            self.instrument.write_termination = TERMINATION_STRING
            self.instrument.timeout = RESOURCE_TIMEOUT
            self.instrument.query = self._send_command
            self.connected = True
        except:
            self.connected = False

        try:
            self.inst_id = RESOURCE_ID+'_'
        except:
            self.inst_id = ''
        
        if RESOURCE_MANAGER == None:
            sys.exit('No VISA resource manager found')
        
    def _send_command(self, command):
        """
        For the waveform generator, you need to write and read twice. The first is to send the command and
        receive confirmation of the command send. The second is to get the result.
        """
        self.instrument.write(command)
        acq = self.instrument.read()
        reply = self.instrument.read()
        
    def fetch(self):
        """Send a command and check if it is positively acknowledged

        :param command: The command to send
        :type command: str
        :raises IOError: if the negative acknowledged or a unknown response
            is returned
        """   
        results = dict()
        id_channel = 1
        try:
            while id_channel <= 2:
                measure = self.instrument.query(':SOUR'+str(id_channel)+':APPL?')[1:-1].split(',')
                results.update({self.inst_id+'wavef_CH'+str(id_channel):[measure.split(' ')[0], '[str] Waveform Channel '+str(id_channel)],
                                self.inst_id+'freq_CH'+str(id_channel):[measure.split(' ')[1], '[Hz] Frequency Channel '+str(id_channel)],
                                self.inst_id+'amp_CH'+str(id_channel):[measure[2], '[V] Amplitude Channel '+str(id_channel)],
                                self.inst_id+'offs_CH'+str(id_channel):[measure[3], '[V] Offset Channel '+str(id_channel)]})
            
                id_channel += 1        
        except:
            [results.update({self.inst_id+'VAR'+str(idx):value}) for idx, value in enumerate(measure)]
        return results
        
    def dump_sensors(self):
        """"""
        results = {**self.fetch()}

        return results