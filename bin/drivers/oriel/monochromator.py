# -*- coding: utf-8 -*-
"""
Created on Wed Mar 3 16:19:00 2021

@author: bserra
"""

import time
import re

REGEX = re.compile(r'[\n\r\t]')

DELAY_QUERIES = 0.05

ERROR_CODES = {'E0000': 'Receive Error',
               'E0001': 'Command Not Recognized',
               'E0002': 'Illegal Parameters',
               'E0100': 'Illegal Move Requested',
               'E0102': 'Illegal Scan Wavelength Parameter',
               'E0200': 'Device Not Available'}

class MS257(object):
    r"""Class that implements the pymont common driver for the MS257
    protocol of the oriel monochromator (now Newport).
    """

    def __init__(self, 
                 RESOURCE_STRING, 
                 RESOURCE_MANAGER = None,
                 RESOURCE_ID = '',
                 RESOURCE_TIMEOUT = 5,
                 TERMINATION_STRING = '>',
                 **kwargs):
        
        """Initialize internal variables and ethernet connection

        :param RESOURCE_STRING: The address of the device
        :type RESOURCE_STRING: str
        :param TERMINATION_STRING: '>' read termination for MS257 protocol
        :type TERMINATION_STRING: str
        """
        try:
            self.instrument = RESOURCE_MANAGER.open_resource(RESOURCE_STRING,
                                                             open_timeout = RESOURCE_TIMEOUT)
            self.instrument.read_termination = TERMINATION_STRING
            #self.instrument.write_termination = ''
            self.instrument.timeout = RESOURCE_TIMEOUT
            self.connected = True
        except:
            self.connected = False

        try:
            self.inst_id = RESOURCE_ID+'_'
        except:
            self.inst_id = ''
        
        if RESOURCE_MANAGER == None:
            sys.exit('No VISA resource manager found')

    def identify(self):
        """Identify the instrument, oriel monochromator don't have
        a readable register with their S/N, so I just take the software
        version (should be 1.72 in lab E0.54)"""
        return 'MS257 Oriel monochromator - Soft v.'+self.instrument.query('?VER') 
       
    def __check_errors(self, instrument_answer):
        """Check the answer for the error codes defined in ERROR_CODES
        Using regex because there is \r\n before every answer from the
        instrument. Nice one oriel...
        """
        # Remove control characters using regular expression
        instrument_answer = REGEX.sub("", instrument_answer)

        if instrument_answer in ERROR_CODES:
            reply = f'{instrument_answer}: {ERROR_CODES[instrument_answer]}'
        else:
            reply = instrument_answer
        return reply
    
    def _get_parameters(self):
        """From a list of commands and description of the answer of the
        instrument, build a list (list_values) of all of those parameters
        """
        list_values = []
        for command, desc in [('?PW', 'Current wavelength selected'),
                              ('?FILT1', 'Filter position of filter wheel 1'),
                              ('?FILT2', 'Filter position of filter wheel 2'),
                              ('?PORTOUT', 'Query output port A for auto, M for Manual'),
                              ('?GRAT', 'Query current grating')]:
            reply = self.__check_errors(self.instrument.query(command))
            time.sleep(DELAY_QUERIES)
            list_values.append((command[1:], reply, desc))
        return list_values
        
     
    def fetch(self):
        """Use self.get_parameters() to retrieve values from the instrument
        """   
        results = dict()
        for var, value, desc in self._get_parameters():
            results.update({self.inst_id+var:[value, desc]})
        return results
        
    def dump_sensors(self):
        """"""
        results = {**self.fetch()}

        return results
