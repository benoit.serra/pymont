## -*- coding: utf-8 -*-
"""
Created on Mon Mar 11 14:52:54 2019
@author: bserra

This module is for controlling and fetching data from the TeePee used at ESO.

Ex: C1 Setpoint 1 (line 245 of the excel file: jumo_list-commands.xlsx) is 0x083C

If I want to ask the value at this register:
TP001|83C?
TP001|083C?
TP001|0x083C?
All three will work

If I want to change it (currently it is at 0, but if I want to change it to 1)
TP001|83C, 1
TP001|083C, 1
TP001|0x083C, 1
"""


import xlrd
import numpy as np
import minimalmodbus
import sys
import struct
import time

# Code translations constants	
# Excel spreadsheet with all codes for JUMO Imago500
#docname = 'jumo_list-commands.xlsx'
docname = 'bin/drivers/jumo/jumo_list-commands.xlsx'

DICT_IMAGO500 = {}
wb = xlrd.open_workbook(docname)
sh = wb.sheet_by_index(0)
address_acc, data_type, description = list(), list(), list()

# For each row in the excel file
for idx, cell_name_val in enumerate(sh.col_values(0)):
	# If the cell is not empty, its a new address to create
	if ('__' not in cell_name_val) and (cell_name_val != ''):
		address_acc, data_type, description = list(), list(), list()
		address_acc.append(sh.col_values(2)[idx])
		data_type.append(sh.col_values(1)[idx])
		description.append(sh.col_values(3)[idx])
		DICT_IMAGO500[cell_name_val.lower()] = {'access':address_acc,
								   'type':data_type,
								   'description':description}
		current_address = cell_name_val
		
	# If the cell is empty, appending to previous address
	elif cell_name_val == '':
		address_acc.append(sh.col_values(2)[idx])
		data_type.append(sh.col_values(1)[idx])
		description.append(sh.col_values(3)[idx])
		DICT_IMAGO500[current_address.lower()] = {'access':address_acc,
									 'type':data_type,
									 'description':description}

class imago500(object):
	r"""Class that implements the common driver for the TeePee 
	(Imago500) measurement and control unit.
	This instrument is controlled through the MODBUS interface (RS422)
	so we can't use pyvisa 
	"""
	def __init__(self,
				RESOURCE_STRING, 
				RESOURCE_MANAGER = None,
				RESOURCE_ID = '',
				RESOURCE_TIMEOUT = 5,
				TERMINATION_STRING = chr(13)+chr(10),
				**kwargs):     
		"""Initialize internal variables and ethernet connection

		:param RESOURCE_STRING: The adress of the Lakeshore
		:type RESOURCE_STRING: str
		:param TERMINATION_STRING: '\r\n' by default for Pfeiffer gauges
		:type TERMINATION_STRING: str
		"""
		# Important for enabling disconnection/connection.
		# if this is not here, the minimalmodbus library will display
		# ' Attempting to use a port that is not open' after reconnect
		minimalmodbus.CLOSE_PORT_AFTER_EACH_CALL = True

		comm_infos = RESOURCE_STRING.split('::')
		try:
			self.instrument = jumo(comm_infos[0].replace('MODBUS',''), int(comm_infos[1]), mode = 'rtu')
			self.instrument.clear_buffers_before_each_transaction = True
			self.instrument.serial.baudrate = 9600
			self.instrument.serial.bytesize = 8
			self.instrument.serial.parity   = 'N' # serial.PARITY_NONE
			self.instrument.serial.stopbits = 1
			self.instrument.serial.timeout  = RESOURCE_TIMEOUT   # seconds	   
			self.instrument._send_command = self.instrument.query
			#self.instrument.debug = True       
			self.connected = True
		except:
			self.connected = False

		try:
			self.inst_id = RESOURCE_ID+'_'
		except:
			self.inst_id = ''
		
		self.JUMO_registers = kwargs['RESOURCE_CONFIG'] 
		if RESOURCE_MANAGER == None:
			sys.exit('No VISA resource manager found')
    
	#def _send_command(self,
	#				address, 
	#				command, 
	#				list_commands = DICT_IMAGO500):
	#	""""""
	#	if isinstance(address,int): 
	#		reply = self.instrument.query(''.join([hex(address), '?']))
	#		#register_modetest = 4
	#		#register_numDec = 2
	#		details_address = list_commands["0x{:04x}".format(address)]
	#		access, rw, desc = [details_address[i] for i in ['access', 'type', 'description']]
	#		#reply = self.instrument.read_register(address, register_numDec, register_modetest)

	#	return (reply, desc)

	def dump_sensors(self):
		""""""
		results = dict()

		for keyw in self.JUMO_registers.keys():
			#print (''.join([keyw, 'I am sending ', str(self.JUMO_registers[keyw]),' to the JUMO']))
			#result = self.instrument._send_command(self.JUMO_registers[keyw], None, DICT_IMAGO500)
			result = self.instrument.query(str(self.JUMO_registers[keyw])+'?')
			results.update({self.inst_id+keyw:(str(result[0]),f"[AU] {result[1][0]}")})
		return results

class jumo(minimalmodbus.Instrument):
	r"""Class inheriting from minimalmodbus.Instrument to add close and query methods needed for
	pymont.
	"""
	def __init__(self, portname, slaveaddress, mode= minimalmodbus.MODE_RTU):
		super(jumo,self).__init__(portname, slaveaddress, mode= mode)

	def close(self):
		r"""Method to close the communication with the JUMO using the 
		CLOSE methods defined in pymont
		"""
		self.serial.close()

	def query(self,
				command, 
				list_commands = DICT_IMAGO500):
		r"""Method to query for registers using the QUERY method in
		the instruction server in pymont
		"""
		if command[-1]=='?' :
			# Register modetest 4 is read
			register_modetest = 4
			register = int(command[:-1], 16)
      # Get details describing this register
			#details_address = list_commands["{0:#0{1}x}".format(register, 6)]
			details_address = list_commands[command[:-1]]
 			# Get the access type, datatype and description associated with this value
			access, dtype, desc = [details_address[i] for i in ['access', 'type', 'description']]
			if dtype[0] == 'FLOAT':
				register_numDec = 2
				reply = self.read_registers(register, register_numDec, functioncode= register_modetest)
			if dtype[0] == 'INT':
				register_numDec = 0
				reply = self.read_register(register, register_numDec, functioncode= register_modetest)
			if (dtype[0] == 'FLOAT'):
				value_hex = struct.pack('>HH', reply[1], reply[0])
				reply = struct.unpack('>f', value_hex)[0] 
        # Doing this to remove values with 200005 when the register are full of shit
				if reply > 500: reply=np.nan
			if (dtype[0] == 'INT') & (len(dtype)>1):
				if '15' in dtype[-1]: n_bit = 16
				elif '7' in dtype[-1]: n_bit = 8
				else: n_bit = None
				
				if n_bit != None:
					format_str = "{0:0"+str(n_bit)+"b}"
					reply = format_str.format(int(reply))
			if (dtype[0] == 'LONG'):
				reply = 'Nothing'
		elif len(command.split(','))==2:
			# Register modetest 6 is write
			register_modetest = 6
			# The first value is the register in hex (base 16)
			register = int(command.split(',')[0], 16)
			# Get details describing this address
			details_address = list_commands["{0:#0{1}x}".format(register, 6)]
			# Get the access type, datatype and description associated with this value
			access, dtype, desc = [details_address[i] for i in ['access', 'type', 'description']]
			if dtype[0] == 'FLOAT':
				register_numDec = 2
				# The second one is the value in a float format 
				value = float(command.split(',')[1])
				try:
					pack_float = struct.pack('>f', value)
					value_registers = struct.unpack('>HH', pack_float)
					self.write_registers(register, list(value_registers)[::-1])
				except ValueError as error:
					reply = command+': not valid'
				reply = self.read_registers(register, register_numDec, 3)
			if dtype[0] == 'INT':
				register_numDec = 0
				# The second one is the value in a integer format 
				value = int(command.split(',')[1])
				try:
					self.write_register(register, value, number_of_decimals = register_numDec, functioncode = register_modetest)
				except ValueError as error:
					reply = command+': not valid'
				reply = self.read_register(register, register_numDec, 3)
		else:
			reply = command+': command not defined'
		return (str(reply), desc)


if __name__ == '__main__':                               
	list_registers = {'SETP_COLD_CPL': '0x0844', 	# C2 - setpoint cold 0x0844
					  'SETP_RT_CPL': '0x0846', 		# C2 - setpoint warm 0x0846
					  'T_DTT': '0x00ca', 			# C1 - filtered process value 0x00CA
					  'H_DTT': '0x00d2', 			# C1 - output heating 0x00D2
					  'SETP_WARM_DTT': '0x083e', 	# DTT - Warm Setpoint 0x083E
					  'SETP_COLD_DTT': '0x083c', 	# DTT - Cold Setpoint 0x083C
					  'T_CPL': '0x00de', 			# C2 - filtered process value 0x00DE
					  'V_CPL': '0x00e6', 			# C2 - output heating 0x00E6
					  'T_EXH': '0x00f2', 			# C3 - filtered process value 0x00F2
					  'H_EXH': '0x00fa', 			# C3 - output heating 0x00FA
					  'SETP_COLD_SPT': '0x0854', 	# C4 - setpoint cold 0x0854
					  'SETP_RT_SPT': '0x0856', 		# C4 - setpoint warm 0x0856
					  'T_SPT': '0x0106', 			# C4 - filtered process value 0x0106
					  'H_SPT': '0x010e',				# C4 - output heating 0x010E
					  'Pressure': '0x00b2'}			# Pressure - Analog input 6 0x00B2
                                    
	connection = imago500(RESOURCE_STRING='MODBUS/dev/ttyUSB1::255::INSTR',
                        RESOURCE_MANAGER='randomresourcemanager',
                        RESOURCE_ID='TP001',
                        RESOURCE_CONFIG=list_registers)
                        
	print (connection.dump_sensors())
	# Change setpoint of COLD_CPL to 110K
	print (connection.instrument.query('0x00b2?'))
	#print (connection.instrument.query('0x0174,512'))
	#time.sleep(5)
	#print (connection.instrument.query('0x0174,256'))
	print ('Day', connection.instrument.query('0x0080?'))
	print ('Month', connection.instrument.query('0x007f?'))
	print ('Year', connection.instrument.query('0x007e?'))
	print ('Seconds', connection.instrument.query('0x0083?'))
    #	connection.instrument.query('0x0844, 100')
#	print (connection.instrument.query('0x0844?'))  
	#print (connection.dump_sensors())
	"""
	connection.instrument.serial.close()
	all_registers = open('all_registers_teepee.txt').readlines()
	for register in all_registers:
		reg = register.strip('\n').lower()
		print (reg, connection.instrument.query(reg+'?'))
	
	print(all_registers)
	"""