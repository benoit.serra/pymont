# -*- coding: utf-8 -*-
"""
Created on Wed May  2 17:45:05 2018

@author: bserra

Problem with no backend avalables
https://stackoverflow.com/questions/13773132/pyusb-on-windows-no-backend-available?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
add filter to usb serial device
"""

import socket
import json
import sys
import time
  ############
#### MDI 23 ###################################################################
  ############


# Code translations constants
IO_ERRORS = {
    '6': 'An I/O os a;ready set to this type. Applies to non-General Purpose I/O',
    '8': 'Tried to set an I/O to an incorrect I/O type.',
    '9': 'Tried to write to I/O set as Input or is “TYPED”.',
    '10': 'Illegal I/O number.',
    '11': 'Incorrect CLOCK type.',
    '12': 'Illegal Trip / Capture.'}

DATA_ERRORS = {
    '20': 'Tried to set unknown variable or flag. Trying to set an undefined variable of flag. Also could be a typo.',
    '21': 'Tried to set an incorrect value. Many variables have a range such as the Run Current (RC) which is 1 to 100%. As an example, you cannot set the RC to 110%.',
    '22': 'VI is set greater than or equal to VM. The Initial Velocity is set equal to, or higher than the Maximum Velocity. VI must be less than VM.',
    '23': 'VM is set less than or equal to VI. The Maximum Velocity is set equal to, or lower than the Initial Velocity. VM must be greater than VI.',
    '24': 'Illegal data entered. Data has been entered that the device does not understand.',
    '25': 'Variable or flag is read only. Read only flags and variables cannot be set.',
    '26': 'Variable or flag is not allowed to be incremented or decremented. IC and DC cannot be used on variables or flags such as Baud and Version',
    '27': 'Trip not defined.Trying to enable a trip that has not yet been defined.',
    '28': 'WARNING! Trying to redefine a program label or variable. This can be caused when you download a program over a program already saved. Before downloading a new r edited program, type <FD> and press ENTER to return the device to the Factory efaults. You may also type <CP> and press ENTER to Clear the Program.',
    '29': 'Trying to redefine a built in command, variable or flag.',
    '30': 'Unknown label or user variable. Trying to Call or Branch to a Label or Variable that has not yet been defined',
    '31': 'Program label or user variable table is full. The table has a maximum capacity of 22 labels and/or user variables.',
    '32': 'Trying to set a label (LB). You cannot name a label and then try to set it to a value. Example: Lable P1 (LB P1 ). The P1 cannot be used to set a variable such as P1=1000.',
    '33': 'Trying to SET an Instruction.',
    '34': 'Trying to Execute a Variable or Flag.',
    '35': 'Trying to Print Illegal Variable or Flag.',
    '36': 'Illegal Motor Count to Encoder Count Ratio.',
    '37': 'Command, Variable or Flag Not Available in Drive.',
    '38': 'Missing parameter separator.',
    '39': 'Trip on Position and Trip on Relative Distance not allowed together.'
    }
    
PROGRAM_ERRORS = {
    '40': 'Program not running. If HOLD (H) is entered in Immediate Mode and a program is not running.',
    '41': 'Stack overflow.',
    '42': 'Illegal program address. Tried to Clear, List, Execute, etc. an incorrect Program address.',
    '43': 'Tried to overflow program stack. Calling a Sub-Routine or Trip Routine with no Return.',
    '44': 'Program locked. User Programs can be Locked with the <LK> command. Once Locked, the program cannot be listed or edited in any way.',
    '45': 'Trying to Overflow Program Space.',
    '46': 'Not in Program Mode.',
    '47': 'Tried to Write to Illegal Flash Address.',
    '48': 'Program Execution stopped by I/O set as Stop.',
    }

COMMUNICATIONS_ERRORS = {
    '60': 'Not used.',
    '61': 'Trying to set illegal BAUD rate. The only Baud Rates accepted are those listed on the Properties Page of IMS Terminal. (4,800, 9,600, 19,200, 38,400, 115,200)',
    '62': 'IV already pending or IF Flag already TRUE.',
    '63': 'Character over-run. Character was received. Processor did not have time to process it and it was over-written by the next character.',
    '64': 'Startup Calibration failed (Hybrid only).',
    }

SYSTEM_ERRORS = {
    '70': 'FLASH Check Sum Fault.',
    '71': 'Internal Temperature Warning, 10C to Shutdown.',
    '72': 'Internal Over TEMP Fault, Disabling Drive.',
    '73': 'Tried to SAVE while moving.',
    '74': 'Tried to Initialize Parameters (IP) or Clear Program (CP) while Moving.',
    '75': 'Linear Overtemperature Error (For units without Internal Over Temp).',
    }

MOTION_ERRORS = {
    '80': 'HOME switch not defined. Attempting to do a HOME (H) sequence but the Home Switch has not yet been defined.',
    '81': 'HOME type not defined. The HOME (HM or HI) Command has been programmed but with no type or an illegal type. (Types = 1, 2, 3, or 4)',
    '82': 'Went to both LIMITS and did not find home. The motion encroached both limits but did not trip the Home switch. Indicates a possible bad switch or a bad circuit.',
    '83': 'Reached plus LIMIT switch. The LIMIT switch in the plus direction was tripped.',
    '84': 'Reached minus LIMIT switch. The LIMIT switch in the minus direction was tripped.',
    '85': 'MA or MR isn’t allowed during a HOME and a HOME isn’t allowed while the device is in motion.',
    '86': 'Stall detected. The Stall Flag (ST) has been set to 1.',
    '87': 'MDrive: In Clock Mode, JOG not allowed. / Hybrid: Not allowed to change AS mode while calibrating.',
    '88': 'MDrive: Following error. / Hybrid: Moves not allowed while calibration is in progress.',
    '89': 'MDrive: Reserved. / Hybrid: Calibration not allowed while motion is in progress.',
    '90': 'Motion Variables are too low switching to EE=1.',
    '91': 'Motion stopped by I/O set as Stop.',
    '92': 'Position Error in Closed loop. motor will attempt tp position the shaft within the deadband, After failing 3 attempts Error 92 will be generated. Axis will continue to function normally.',
    '93': 'MR or MA not allowed while correcting position at end of previous MR or MA.',
    '94': 'Clear Locked Rotor Fault not allowed while in Motion. MDrive Hybrid products(MAI) only.',
    }
    
HYBRID_ERRORS = {
    '100': 'Configuration test done, encoder resolution mismatch.',
    '101': 'Configuration test done, encoder direction incorrect.',
    '102': 'Configuration test done, encoder resolution and direction incorrect.',
    '103': 'Configuration not done, drive not enabled.',
    '104': 'Locked rotor.',
    '105': 'Maximum position count reached.',
    '106': 'Lead limit reached.',
    '107': 'Lag limit reached.',
    '108': 'Reserved.',
    '109': 'Calibration failed.',
    }    
    
err_list = [IO_ERRORS,
    DATA_ERRORS,
    PROGRAM_ERRORS,
    COMMUNICATIONS_ERRORS,
    SYSTEM_ERRORS,
    MOTION_ERRORS,
    HYBRID_ERRORS]  

 
class MDI23(object):
    r"""Abstract class that implements the common driver for the model MDrive
    23 control drive. The driver implement the following x commands out 
    the x in the specification:

    """
    def __init__(self, 
                 RESOURCE_STRING, 
                 RESOURCE_MANAGER = None,
                 RESOURCE_ID = '',
                 RESOURCE_TIMEOUT = 5,
                 TERMINATION_STRING = '\r\n',
                 **kwargs):
        
        """Initialize internal variables and ethernet connection

        :param RESOURCE_STRING: The adress of the agilent
        :type RESOURCE_STRING: str
        :param TERMINATION_STRING: '\r\n' by default for schneider
        :type TERMINATION_STRING: str
        """
        try:
            self.instrument = RESOURCE_MANAGER.open_resource(RESOURCE_STRING,
                                                             open_timeout = RESOURCE_TIMEOUT)
            self.instrument.read_termination = TERMINATION_STRING
            self.instrument.write_termination = TERMINATION_STRING
            self.instrument.timeout = RESOURCE_TIMEOUT
            self.connected = True
        except:
            self.connected = False
            
        self.positions = []
        self._init_params()
        schneider_sn = self.instrument.query('PR SN')
        self.positions = kwargs['RESOURCE_MODEL'][schneider_sn]
        
        try:
            self.inst_id = RESOURCE_ID+'_'
            schneider_sn = self.instrument.query('PR SN')
            self.instrument.positions = kwargs['RESOURCE_MODEL'][schneider_sn]
        except:
            self.inst_id = ''
        
        if RESOURCE_MANAGER == None:
            sys.exit('No VISA resource manager found')
    
    def _init_params(self):
        """"""
        self._send_command('ER 0')
        time.sleep(.5)
        self._send_command('VM 10000')
        time.sleep(.5)
        self._send_command('S3 1,1,0')
        time.sleep(.5)
    
#    def close(self):
#        """Stop the connection to the MDI23 motor"""
#        self.instrument.close()
#        self.connected = False

    def connect(self):
        """Start connection with schneider"""
        self.__init__()

    def __test_cmd(self,cmd):
        """Testing command raw output"""
        results = self.instrument.query(cmd)
        return results

    def _ack_cmd(self):
        """"""
        return None

    def _send_command(self, command):
        """Send a command and check if it is positively acknowledged

        :param command: The command to send
        :type command: str
        :raises IOError: if the negative acknowledged or a unknown response
            is returned
        """
        data = None
        if self.connected == True:
            if (command in [position.split(':')[0] for position in self.positions]):
                cmd_tosend = [position.split(':')[-1] for position in self.positions if command in position]
                self.instrument.write(cmd_tosend[0])
                data = 'Executing '+cmd_tosend[0]
            else:
                self.instrument.write(command)
            if ('PR' in command) & ('PR AL' != command):
                data = self.instrument.read()
                
            error = self.instrument.query('PR ER')

#                if data[:-1] == command:
#                    data = self.instrument.read()
#            elif ('PR AL' == command):
#                print ('2nd')
#                while True:
#                    data = self.socket.recv(self.BUFFER_SIZE*4)
#                    print (data)
#                    if data == '':
#                        break
#           
            if (error != '') & (error != '0'):
                data = [command+':'+error+' - '+err_dict[error]\
                        for err_dict in err_list\
                        if error in err_dict.keys()][0]
                            
        else:
            data = 'notconnected'

        return data
        
    def fact_reset(self):
        """"""
        if self.connected == True:
            self.socket.send(self._cr_lf('FD').encode())
#            self.socket.recv(self.BUFFER_SIZE).decode('unicode_escape')
            self.socket.close()
            self.__init__()
        return 'pouet'
    
    def dump_sensors(self):
        """"""
        
        results1 = self._send_command('PR P')
        results2 = self._send_command('PR MV')
        results = {self.inst_id+'POSI':[results1.replace('\r\n', ''), '[steps] Number of steps from origin'],
                   self.inst_id+'MOVE':[results2.replace('\r\n', ''), '[bool] Is the motor moving']}

        return results

class FilterWheel(MDI23):
    """Driver for the MDI 23 motor of the filter wheel in CRISLER"""
    def __init__(self, TCP_IP='192.168.33.2', TCP_PORT=503, BUFFER_SIZE=1024,**kwargs):
        """Initialize internal variables and serial connection

        :param port: The COM port to open. See the documentation for
            `pyserial <http://pyserial.sourceforge.net/>`_ for an explanation
            of the possible value. The default value is '/dev/ttyUSB0'.
        :type port: str or int
        :param baudrate: 9600, 19200, 38400 where 9600 is the default
        :type baudrate: int
        """
        super(FilterWheel, self).__init__(TCP_IP      = TCP_IP,
                                    TCP_PORT    = TCP_PORT,
                                    BUFFER_SIZE =BUFFER_SIZE,
                                    **kwargs)

    r"""The Fitler wheel class with specific functions for the filter wheel"""
    def moveto(self, position):
        """"""
        if position in FilterWheel.FILTER_POS.keys():
            self._send_command(FilterWheel.FILTER_POS[position])
            print (self._send_command('PR MV'))
        else:
            return str(position)+': is not a valid position for the motor'   
