"""This module contains drivers for the following equipment from Pfeiffer
Vacuum:

* TPG 262 and TPG 261 Dual Gauge. Dual-Channel Measurement and Control
    Unit for Compact Gauges
    
Program from python library PyExpLabSys
https://github.com/CINF/PyExpLabSys/blob/master/PyExpLabSys/drivers/pfeiffer.py
adding .encode() or .decode() to convert unicode str/bytes 
(serial wants only bytes in P3.x)

Changes for connection using PYVISA
"""

import time
import sys

# Code translations constants
MEASUREMENT_STATUS = {
    0: 'Measurement data okay',
    1: 'Underrange',
    2: 'Overrange',
    3: 'Sensor error',
    4: 'Sensor off (IKR, PKR, IMR, PBR)',
    5: 'No sensor (output: 5,2.0000E-2 [mbar])',
    6: 'Identification error'
}

PRESSURE_UNITS = {0: 'mbar/bar', 1: 'Torr', 2: 'Pascal'}

#### ADC_MK?? ##################################################################

class ADC_MKII(object):
    r"""Abstract class that implements the common driver for the TPG 261 and
    ADC MKIII dual channel measurement and control unit. The driver implements
    the following x commands out the x in the specification:

    """

    ETX = chr(3)  # \x03
    CR = chr(13)
    LF = chr(10)
    ENQ = chr(5) # \x05
    ACK = chr(6)  # \x06
    NAK = chr(21)  # \x15

    def __init__(self, 
                 RESOURCE_STRING, 
                 RESOURCE_MANAGER = None,
                 RESOURCE_ID = '',
                 RESOURCE_TIMEOUT = 5,
                 TERMINATION_STRING = chr(13),
                 **kwargs):
        
        """Initialize internal variables and ethernet connection

        :param RESOURCE_STRING: The adress of the Lakeshore
        :type RESOURCE_STRING: str
        :param TERMINATION_STRING: '\n' by default for Pfeiffer gauges
        :type TERMINATION_STRING: str
        """

        try:
            self.instrument = RESOURCE_MANAGER.open_resource(RESOURCE_STRING,
                                                             open_timeout = RESOURCE_TIMEOUT)
            self.instrument.read_termination = TERMINATION_STRING
            self.instrument.write_termination = TERMINATION_STRING
            self.instrument.timeout = RESOURCE_TIMEOUT
            self.connected = True
        except:
            self.connected = False

        try:
            self.inst_id = RESOURCE_ID+'_'
        except:
            self.inst_id = ''
        
        if RESOURCE_MANAGER == None:
            sys.exit('No VISA resource manager found')

    def _send_command(self, command):
        """Send a command and check if it is positively acknowledged

        :param command: The command to send
        :type command: str
        :raises IOError: if the negative acknowledged or a unknown response
            is returned
        """
        response = self.instrument.query(command)
        return response

    def _get_data(self):
        """Get the data that is ready on the device

        :returns: the raw data
        :rtype:str
        """
        return self.instrument.query(self.ENQ)

    def _clear_output_buffer(self):
        """Clear the output buffer"""
        time.sleep(0.1)
        just_read = 'start value'
        out = ''
        while just_read != '':
            just_read = self.serial.read()
            out += just_read
        return out

    def program_number(self):
        """Return the firmware version

        :returns: the firmware version
        :rtype: str
        """
        self._send_command('?GV1')
        return self._get_data()


    def pressure_gauge(self, gauge=1):
        """Return the pressure measured by gauge X

        :param gauge: The gauge number, 1 or 2
        :type gauge: int
        :raises ValueError: if gauge is not 1 or 2
        :return: (value, (status_code, status_message))
        :rtype: tuple
        """
        if gauge not in [1, 2]:
            message = 'The input gauge number can only be 1 or 2'
            raise ValueError(message)
        self._send_command('?GA' + str(gauge))
        reply = self._get_data()
        status_code = int(reply.split(',')[0])
        value = float(reply.split(',')[1])
        return value, (status_code, MEASUREMENT_STATUS[status_code])


    def dump_sensors(self):
        """Return the pressures measured by the gauges

        :return: (value1, (status_code1, status_message1), value2,
            (status_code2, status_message2))
        :rtype: tuple
        """
        results = dict()
        for command in ['?GA1', '?GA2']:
                reply = self._send_command(command)
                results.update({self.inst_id+command:[reply,'mbar - Pressure gauge']})

        return results

    def gauge_identification(self):
        """Return the gauge identication

        :return: (id_code_1, id_1, id_code_2, id_2)
        :rtype: tuple
        """
        self._send_command('?GV')
        reply = self._get_data()
        id1, id2 = reply.split(',')
        return id1, GAUGE_IDS[id1], id2, GAUGE_IDS[id2]


    def pressure_unit(self):
        """Return the pressure unit

        :return: the pressure unit
        :rtype: str
        """
        self._send_command('?US')
        unit_code = int(self._get_data())
        return PRESSURE_UNITS[unit_code]


    def rs232_communication_test(self):
        """RS232 communication test

        :return: the status of the communication test
        :rtype: bool
        """
        self._send_command('RST')
        self.serial.write(self.ENQ)
        self._clear_output_buffer()
        test_string_out = ''
        for char in 'a1':
            self.serial.write(char)
            test_string_out += self._get_data().rstrip(self.ENQ)
        self._send_command(self.ETX)
        return test_string_out == 'a1'

    def close(self):
        self.serial.close()

class vacgauge(ADC_MKII):
    """Driver for the ADC MKII dual channel measurement and control unit"""

    def __init__(self, port='COM1', baudrate=9600,**kwargs):
        """Initialize internal variables and serial connection

        :param port: The COM port to open. See the documentation for
            `pyserial <http://pyserial.sourceforge.net/>`_ for an explanation
            of the possible value. The default value is '/dev/ttyUSB0'.
        :type port: str or int
        :param baudrate: 9600, 19200, 38400 where 9600 is the default
        :type baudrate: int        
        """
        super(vacgauge, self).__init__(port=port, baudrate=baudrate,**kwargs)



