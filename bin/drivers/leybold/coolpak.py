# -*- coding: utf-8 -*-
"""
A library for communicating with Leybold COOLPAK 6000 compressor units
using the serial RS232 interface
Protocol data format is 4800 baud 8N1 (8 bit, no parity check, 1 stop bit)
"""
__author__ = "Joonas Konki"
__license__ = "MIT, see LICENSE for more details"
__copyright__ = "2018 Joonas Konki"

import visa
import logging
import serial
import time

# Set the minimum safe time interval between sent commands that is required according to the user manual
START_CHR = '\x02'
END_CHR   = '\x0D'

# Interpretation of the status character string response from the unit
INTERP = {
        'SWV' : 'Software version',
        'LIU1' : 'Leybold internal use only',
        'CHC' : 'COOLPAK hours counter',
        'LIU2' : 'Leybold internal use only',
        'LIU3' : 'Leybold internal use only',
        'LIU4' : 'Leybold internal use only',
        'ONDelay' : 'Switch-ON delay timer [seconds]',
        'COM_STATUS' : 'Command status (0==OFF, 1==ON, 2==SYSTEM ERROR)',
        'STATUS' : 'Actual compressor status (0==OFF, 1==ON)',
        'NA1' : 'Not used',
        'NA2' : 'Not used',
        'NERRORS' : 'Number of still active errors',
        'ERRORS' : 'Error bit map (1==error active, 0==error not active)',
        'NErrorsLog' : 'Number of recorded and logged errors'
}

class Coolpak6000(object):
	r"""Class that implements the common driver for the Leybold compressor Coolpak6000
	"""
	def __init__(self,
				RESOURCE_STRING, 
				RESOURCE_MANAGER = None,
				RESOURCE_ID = '',
				RESOURCE_TIMEOUT = 5,
				TERMINATION_STRING = '\r',
				**kwargs):		 
		"""Initialize internal variables and ethernet connection

		:param RESOURCE_STRING: The adress of the Lakeshore
		:type RESOURCE_STRING: str
		:param TERMINATION_STRING: '\r\n' by default for Pfeiffer gauges
		:type TERMINATION_STRING: str
		"""

		comm_infos = RESOURCE_STRING.split('::')
		try:
			self.instrument = RESOURCE_MANAGER.open_resource(RESOURCE_STRING, baud_rate = 4800)
			self.instrument.write_termination = TERMINATION_STRING
			self.instrument.read_termination = TERMINATION_STRING
			self.instrument.query = self._send_command
			self.connected = True
			self.startstop = {'START':self.start,
						 'STOP':self.stop}
		except Exception as e:
			print (e)
			self.connected = False

		try:
			self.inst_id = RESOURCE_ID+'_'
		except:
			self.inst_id = ''

	def __format_cmd(self, string):
		""""""
		fmt_cmd = f'{START_CHR}{string}{END_CHR}'.encode('ascii') 
		return fmt_cmd

	def _send_command(self, 
					  command):
		""""""
		if command in self.startstop:
			response = self.startstop[command]()
			reply = response
		else:
			self.instrument.write_raw(self.__format_cmd(command))
			time.sleep(1)
			reply = self.instrument.read()#_bytes(72)
		return reply  
  
	def identify(self):
		""""""
		reply = self._send_command(0x1F, 2)
		return reply.registers
	
	def start(self):
		try:
			self.instrument.write_raw(self.__format_cmd('SYS1'))
			reply = self.instrument.read()
		except:
			reply = f"Sent start (SYS1) but no reply"
		return reply

	def stop(self):
		try:
			self.instrument.write_raw(self.__format_cmd('SYS0'))
			reply = self.instrument.read()
		except:
			reply = f"Sent start (SYS0) but no reply"
		return reply

	def dump_sensors(self):
		""""""
		results = dict()
		reply = self.instrument.query('DAT')
		# Software version is right next to \x02DAT to split the reply by data and get the right side
		data = reply.split('DAT')[1].split('/')
		for idx, name in enumerate(INTERP):
			results.update({self.inst_id+name:[data[idx], INTERP[name]]})
		return results

if __name__ == "__main__":
	rm = visa.ResourceManager()
	comp = Coolpak6000("ASRL/dev/ttyUSB1::INSTR",
					   RESOURCE_ID = 'CC001',
					   RESOURCE_MANAGER = rm)
	print (comp.connected)
	reply = comp.dump_sensors()
	print (reply)
	#comp.set_power_on()
	#time.sleep(1)
	#comp.set_power_off()
	#comp.start()
	#time.sleep(2)
	#comp.stop()
