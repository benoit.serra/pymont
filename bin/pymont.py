# -*- coding: utf-8 -*-
"""
Created on Tue Apr  3 11:12:03 2018

@author: serra

v1.1.2 - Adding Jumo Imago 500 to the jumo drivers.\r\n

v1.2 - \n
Changing software architecture with separate classes\r\n

issue 3 - Possibility to disconnect one instrument from the actual config\r\n
issue 3 - When all instruments disconnected, can change the config file\r\n
issue 4 - Logger used for everything\r\n
issue 5 - Mailer in case of problem?\r\n
"""

# pymont.py --- Simple application for monitoring Cryostats
#
# Copyright (C) 2018-2019 Benoit Serra
#
# This file is a program monitoring the instruments connected to it.
# It may be used and modified with no restriction; raw copies as well
# as modified versions may be distributed without limitation.

# IMPORTS
#########################
from __future__ import unicode_literals
import sys
import os

import numpy as np
import time

import argparse
import logging

from classes.opcuaasync_server import OPCServer

progname = os.path.basename(sys.argv[0])
progversion = "1.4"

# METHODS
#########################

# MAIN
#########################
if __name__ == "__main__":

    # Initializing the parser
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                     description=__doc__)

    parser.add_argument('-v', '--verbosity', action='count', default=0, help='Increase output verbosity (-v/-vv/-vvv)')
    parser.add_argument('-V','--version', action='version',
                        version='Pymont, version {version}'.format(version=progversion))
    parser.add_argument('-c','--config', type=str, required=True,
                        help='Configuration file to load (json)')
    parser.add_argument('-cli','--command-interface',action='store_true')
    parser.add_argument('-gui','--graphical-interface',action='store_true')

    # Parsing inputs arguments
    opts = parser.parse_args()

    # Initializing the logging
    logging_level = [logging.ERROR, logging.WARNING, logging.INFO, logging.DEBUG][min(opts.verbosity, 3)]
    log_format = '%(asctime)s - %(name)s - %(funcName)20s \t %(message)s'
    logging.basicConfig(level=logging_level, format=log_format, datefmt='%Y-%m-%d_%H-%M-%S')

    if opts.graphical_interface:
        print ('GUI version not supported in 1.0. Exiting.')
    elif opts.command_interface:
        if opts.config:
            OPCServer(config_filename = opts.config)
        else:
            sys.exit('Error: no config file defined')
    else:
        print ('No gui or cli argument, leaving')

# GARBAGE
#########################
