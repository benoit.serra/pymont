# conda environment name
# instrumentation configuration file
# conda_env="pymont-env"
# instrumentation configuration file
config_file="lab54-1_config.yaml"
# IP address and port for the instruction serv saved in the conf. file
is_address=`cat config/$config_file | grep host | grep -o '".*"' | cut -c 8- | sed 's/\"//g'`

# activating the conda environment
echo "Activating conda environment "$conda_env" ..."
#source activate $conda_env
# echo "Activating conda environment "$conda_env" ..."
# source activate $conda_env

# starting pymont in Command Line Interface (CLI), redirect program outputs to the logs/ directory
echo "Starting pymont with "$config_file" configuration file"
python3 bin/pymont.py -vvv -cli -c $config_file #&> "logs/$(date +%F_%H-%M-%S)_pymontlog.log" &

# starting the bokeh server, redirect outputs to the logs/ directory
#bokeh serve --log-level debug bin/pymont_display.py --allow-websocket-origin=134.171.12.57:5006 &> "logs/$(date +%F_%H-%M-%S)_pymontlog.log" &

# printing information on current instance of pymont
echo "Pymont started, connect to the instruction server: "$is_address
echo "Process running pymont:"
ps -aux | grep pymont.py | head -n 1

# deactivating python environment
echo "Deactivating conda environment "$conda_env" ..."
source deactivate


