PYMONT
******

.. _introduction:

PYthon MONitoring Tool [PYMONT]
===================================

This documentation covers the setup and operation of the monitoring tool 
PYMONT used for the cryogenic test benches in the detector group at ESO.

The goal is to provide a basic understanding of the capabilities of the 
tool as well as how to configure and start it.

How does Pymont works?
===================================
 
The core of Pymont lies on the Communication Server (CS). This object
allows communication with all instruments, given that the a pymont-compatible
driver is present (how to write a driver in contribution guide).
The communication server relies on several libraries for communication:
  - PyVISA: GPIB, USB, RS232, Ethernet (TCP/IP)
  - PyModbus: Modbus Ethernet
  - minimalmodbus: Modbus RS485 (to be changed to PyModbus)
  - http requests: IO-Link

The initialization of the communication server is reliant on a YAML
configuration file which contains the information for the connection and
an identifier string for types of instruments.

Once the communication server is ready, Pymont is starting an OPC-UA server. 
This server is using asyncua to establish a main event loop where 
instruction are stacked as coroutine until execution.
This allows user to stack instructions for the server and wait properly for
an answer from instrument before starting another instruction.
The OPC-UA server is using the dump_variable from the pymont driver 
(every x seconds) to expose this data in variable nodes in the server. 
The data is then available for scrapping for monitoring/saving purposes.

To send instructions you need the asyncua library to create an async
client to connect to the OPC-UA server (example in pymont-utils: 
https://gitlab.eso.org/bserra/pymont-utils/).
Once connected, you can send a list of commands that will be executed 
sequentially. 

