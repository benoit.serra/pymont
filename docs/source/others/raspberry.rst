.. _raspberry:

Installing Pymont on a Raspberry Pi
===================================

Some random text

.. _os-installation:

Installing the OS
-----------------
Format the SD card in FAT32 (quick format)


Copy NOOBS contents into the sd card

Plug the SD card in the Raspberry and boot it. Check the options before installing the OS and start.

COFFEE TIME

For the first session, set a password

Go to the start menu: Preferences > Raspberry Pi Configuration
Change the boot to CLI (command line interface)

Reboot

Create user 

irlabf with same access as pi

.. code-block:: bash
	
	$ sudo adduser <USERNAME>
	$ sudo visudo

Use the cursor keys to navigate to the line below the entry for pi and copy it exactly, but use your new username instead of pi. 

.. code-block:: bash

	pi    ALL=(ALL) ALL
	<USERNAME>  ALL=(ALL) ALL

Ctro o ctrl X to save and close editor

Logout then login with newuser irlab

.. code-block:: bash

	$ logout

.. _network-config:

Network configuration
---------------------
Change the /etc/dhcpcd.conf file to add the following content at the end of the file (nano is installed on the base version of NOOBS).

.. code-block:: bash

	$ nano /etc/dhcpcd.conf 
	
.. code-block:: bash
	
	interface eth0

	static ip_address=192.168.7.190/24
	static routers=192.168.7.254
	static domain_name_servers=192.168.7.11 192.168.15.11

If the /24 is not in the static ip address, the raspberry will be able to ping instruments in subnet 5. But other subnets will not be able to access the raspberry because it is on the wrong gateway.

It is also recommended to change the following file /etc/network/interfaces by adding the following lines

.. code-block:: bash

	$ nano /etc/network/interfaces 
	
.. code-block:: bash

	auto lo
	iface lo inet loopback

	auto eth0
	iface eth0 inet static
	   address 192.168.7.190/24
	   netmask 255.255.255.0
	   network 192.168.7.0
	   gateway 192.168.7.254
	   dns-nameserver 192.168.7.11
	   dns-nameserver 192.168.15.11

Then restart the networking service

.. code-block:: bash
	
	$ sudo ifconfig eth0 up
	$ sudo ifconfig eth0 down
	$ sudo service networking restart
	$ ifconfig

If problems check the ifconfig, it should look like this

.. code-block:: bash

	$ ifconfig

.. code-block:: bash
	
	eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
       inet 192.168.7.190  netmask 255.255.255.0  broadcast 192.168.7.255
       inet6 fe80::ba27:ebff:fe47:7157  prefixlen 64  scopeid 0x20<link>
       ether b8:27:eb:47:71:57  txqueuelen 1000  (Ethernet)
       RX packets 187  bytes 18949 (18.5 KiB)
       RX errors 0  dropped 0  overruns 0  frame 0
       TX packets 169  bytes 24257 (23.6 KiB)
       TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

If the IP address is allowed to have access to internet, you should be able to update the debian kernal

.. _ssh-config:

Enabling SSH services
---------------------

.. code-block:: bash
	
	$ sudo service ssh start
	$ sudo service ssh status

To start automatically the ssh server when booting up you must use

.. code-block:: bash

	$ sudo update-rc.d ssh defaults
	$ sudo systemctl enable ssh.service
	
You can then connect to the raspberry using putty

.. note::
	A useful command to see which services are started is
	
	.. code-block:: bash

		$ service --status-all

.. _system-updates:

Updating the kernel
---------------------

.. code-block:: bash

	$ sudo apt-get update
	$ sudo apt-get upgrade
	$ wget 'https://raw.githubusercontent.com/notro/rpi-source/master/rpi-source' -O /usr/bin/rpi-source && sudo chmod +x /usr/bin/rpi-source && /usr/bin/rpi-source -q --tag-update
	$ sudo apt-get install tk-dev build-essential texinfo texi2html libcwidget-dev libncurses5-dev libx11-dev binutils-dev bison flex libusb-1.0-0 libusb-dev libmpfr-dev libexpat1-dev tofrodos subversion autoconf automake libtool mercurial bc
	$ sudo apt-get update
	$ sudo apt-get upgrade
	$ sudo rpi-update
	$ sudo reboot

Reconnect after reboot

.. code-block:: bash

	$ sudo rpi-source --skip-gcc

.. important::
	
	After installing the packages, another set of update/ugrade might be needed before the rpi-source execution
	
	.. code-block:: bash

		$ sudo apt-get update
		$ sudo apt-get upgrade	

.. _python-install:

Python install
---------------------

The tested versio of anaconda was `Berryconda <https://github.com/jjhelmus/berryconda>`_. for raspberry 3 (python 3.6)


.. code-block:: bash

	$ wget 'https://github.com/jjhelmus/berryconda/releases/download/v2.0.0/Berryconda3-2.0.0-Linux-armv7l.sh' 
	$ bash Berryconda3-2.0.0-Linux-armv7l.sh
	$ source .bashrc

Creating a new environment is then easy using conda commands

.. code-block:: bash

	$ conda create -n <NAME> python=3.6
	$ source activate <NAME>

All that is left is to install the libraries in the virtual environment

.. code-block:: bash

	$ pip install pyserial, numpy, pyvisa, pyvisa-py, pyusb

.. _gpib-drivers-install:

GPIB drivers installation
-------------------------

You must install `linux-gpib <https://www.cl.cam.ac.uk/~osc22/tutorials/gpib_usb_linux.html>`_.

.. code-block:: bash

    $ wget 'https://sourceforge.net/projects/linux-gpib/files/latest/download/linux-gpib-4.1.0.tar.gz'
    $ tar -xvf linux-gpib-4.1.0.tar.gz
    $ cd linux-gpib-4.1.0
    $ ./configure
    $ make
    $ make install

Then you must bind the gpib python library  to the current python environment <NAME>

.. code-block:: bash

	$ cd language/python
	$ python setup.py install
	$ sudo ldconfig
	$ sudo gpib_config -f /etc/gpib.conf
	
Then, using the pyvisa you can see the possible communication protocol that you can use:

.. code-block:: bash

	$ python -m visa info
	
The following lines should be displayed if the gpib has been properly installed

.. code-block:: bash

	Machine Details:
	   Platform ID:    Linux-4.14.62-v7+-armv7l-with-debian-9.4
	   Processor:

	Python:
	   Implementation: CPython
	   Executable:     <PATHTOPYTHONEXE>
	   Version:        <PYTHONVERS>
	   Compiler:       GCC 6.3.0 20170516
	   Bits:           32bit
	   Build:          Jul 29 2018 08:27:12 (#default)
	   Unicode:        UCS4

	PyVISA Version: 1.9.1

	Backends:
	   ni:
		  Version: 1.9.1 (bundled with PyVISA)
		  Binary library: Not found
	   py:
		  Version: 0.3.0
		  ASRL INSTR: Available via PySerial (3.4)
		  USB INSTR:
			 Please install PyUSB to use this resource type.
			 No module named 'usb'
		  USB RAW:
			 Please install PyUSB to use this resource type.
			 No module named 'usb'
		  TCPIP INSTR: Available
		  TCPIP SOCKET: Available
		  GPIB INSTR: Available via Linux GPIB (b'4.1.0')

Then you need to change the permission to read/write to the gpib:

.. code-block:: bash

	$ chmod o+rw /dev/gpib0


.. _gpib-adapters:

National instrument
+++++++++++++++++++

Change /etc/gpib.conf file (nano editor included)
Replace the board by ni_usb_b for NI GPIB connector

Agilent
+++++++

82357A
''''''

First time the adaptor is plugged to the instrument, the red light 
indicator will light up. This means that the firmware has not been 
uploaded. The communication will not work until the firmware is 
installed.

Install fxload on the raspberry:

.. code-block:: bash

	$ sudo apt-get install fxload

Download the firmware tarball and unpack it

.. code-block:: bash

	$ wget http://linux-gpib.sourceforge.net/firmware/gpib_firmware-2008-08-10.tar.gz
	$ tar xvzf gpib_firmware-2008-08-10.tar.gz
	$ cd gpib_firmware-2008-08-10/agilent_82357a

Verify the address of the adaptor using lsusb

.. code-block:: bash

	$ lsusb
	Bus 001 Device 007: ID 0957:0518 Agilent Technologies, Inc. 82357B GPIB Interface
	Bus 001 Device 004: ID 0403:6001 Future Technology Devices International, Ltd FT232 USB-Serial (UART) IC
	Bus 001 Device 006: ID 0424:7800 Standard Microsystems Corp.
	Bus 001 Device 003: ID 0424:2514 Standard Microsystems Corp. USB 2.0 Hub
	Bus 001 Device 002: ID 0424:2514 Standard Microsystems Corp. USB 2.0 Hub
	Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
	
Upload the firmware using fxload

.. code-block:: bash

	$ sudo fxload -t fx2 -D /dev/bus/usb/001/007 -I ./measat_releaseX1.8.hex 

Verify the address using lsusb and repeat the previous step, as the 
device number might have changed

.. code-block:: bash

	$ lsusb
	Bus 001 Device 008: ID 0957:0518 Agilent Technologies, Inc. 82357B GPIB Interface
	Bus 001 Device 004: ID 0403:6001 Future Technology Devices International, Ltd FT232 USB-Serial (UART) IC
	Bus 001 Device 006: ID 0424:7800 Standard Microsystems Corp.
	Bus 001 Device 003: ID 0424:2514 Standard Microsystems Corp. USB 2.0 Hub
	Bus 001 Device 002: ID 0424:2514 Standard Microsystems Corp. USB 2.0 Hub
	Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub


.. code-block:: bash

	$ sudo fxload -t fx2 -D /dev/bus/usb/001/008 -I ./measat_releaseX1.8.hex

LEDs on the adaptor should both be green and lsusb return a different 
output:

.. code-block:: bash

	$ lsusb
	Bus 001 Device 009: ID 0957:0718 Agilent Technologies, Inc.
	Bus 001 Device 004: ID 0403:6001 Future Technology Devices International, Ltd FT232 USB-Serial (UART) IC
	Bus 001 Device 006: ID 0424:7800 Standard Microsystems Corp.
	Bus 001 Device 003: ID 0424:2514 Standard Microsystems Corp. USB 2.0 Hub
	Bus 001 Device 002: ID 0424:2514 Standard Microsystems Corp. USB 2.0 Hub
	Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub

Now the adaptor is working and you can use your instrument using ibtest 
on port 12 (bold green represent the inputs)

.. code-block:: bash

	$ ibtest
	Do you wish to open a (d)evice or an interface (b)oard?
			(you probably want to open a device): d
	enter primary gpib address for device you wish to open [0-30]: 12
	trying to open pad = 12 on /dev/gpib0 ...
	You can:
			w(a)it for an event
			write (c)ommand bytes to bus (system controller only)
			send (d)evice clear (device only)
			change remote (e)nable line (system controller only)
			(g)o to standby (release ATN line, system controller only)
			send (i)nterface clear (system controller only)
			ta(k)e control (assert ATN line, system controller only)
			get bus (l)ine status (board only)
			go to local (m)ode
			change end (o)f transmission configuration
			(q)uit
			(r)ead string
			perform (s)erial poll (device only)
			change (t)imeout on io operations
			request ser(v)ice (board only)
			(w)rite data string
	: w
	enter a string to send to your device: *IDN?
	sending string: *IDN?

	gpib status is:
	ibsta = 0x2100  < END CMPL >
	iberr= 0

	ibcnt = 6
	You can:
			w(a)it for an event
			write (c)ommand bytes to bus (system controller only)
			send (d)evice clear (device only)
			change remote (e)nable line (system controller only)
			(g)o to standby (release ATN line, system controller only)
			send (i)nterface clear (system controller only)
			ta(k)e control (assert ATN line, system controller only)
			get bus (l)ine status (board only)
			go to local (m)ode
			change end (o)f transmission configuration
			(q)uit
			(r)ead string
			perform (s)erial poll (device only)
			change (t)imeout on io operations
			request ser(v)ice (board only)
			(w)rite data string
	: r
	enter maximum number of bytes to read [1024]: 1024
	trying to read 1024 bytes from device...
	received string: 'LSCI,MODEL340,341677,042304'
	
For plug and play, you need to copy the firmware to /usr/share/usb/agilent

.. code-block:: bash

	$ cp measat_releaseX1.8.hex /usr/share/usb/agilent_82357a/

Otherwise, everything needs to be done once again when the USB is connected
For each reboot

.. code-block:: bash

	$ chmod o+rw /dev/gpib0
	$ sudo ldconfig
	$ sudo gpib_config

For agilent GPIB-USB adapter 82357a this `tutorial <https://www.linuxquestions.org/questions/linux-software-2/how-to-enable-agilent-82357a-usb-gpib-dongle-and-remain-sane-4175498814/>`_. was used 

For raspberry 3 I did put some line in .bashrc file to activate the gpib dongle 82357a everytime its rebooted







