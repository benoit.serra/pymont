References
***********

Some ressources were taken from other projects:

Leybold Coolpak 6000 communication:
https://github.com/jopekonk/coolpaklib

Cryomech CPA compressors:
https://github.com/Cryomech/SampleCode

Pfeiffer TPG controller for vacuum gauges:
https://github.com/CINF/PyExpLabSys/blob/master/PyExpLabSys/drivers/pfeiffer.py

