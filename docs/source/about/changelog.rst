Change log
***********

.. _changelog:

:v0.1: Embedding pyqtgraph in the interface, communication tab with each
       instrument

------------ 

:v0.2: Changing layout to add a splitter in order to resize the graph/commands

------------ 

:v0.3: - Add support for second lakeshore controller
       - Added a configuration file (instrumentation_config.json)
       - Added dynamical pen, linestyle change with type of instr., color change
         for different sensors		
:v0.3.1: - Pressure Y axis now in log scale
         - Line width set to 3
         - Adding a more appropriate message to the About info box
         - Status bar now display name+version of soft
         - X grid displayed on plots init

------------ 

:v0.4:	- Adding UPS communication
:v0.4.1: - Adding Multimeter rhode&schwarze communication (HMC8012)
         - Adding Power Supply rhode&schwarze communication (HMP4040)
:v0.4.2: - Adding switching mechanism for choosing which instrument we want to use
:v0.4.3: - Adding a last cooldown directory creation in the file menu for support
           of the bokeh server
         - Adding an instruction server which redirect request to instruments
           on a separate thread [class InstructionServer():]
           host='134.171.36.18', port=4500
         - Adding oscilloscope Agilent DSO5034A
           host='134.171.5.184', port=5025
         - Adding power supply Rigol DP831A (replacing R&S HMP4040)
           serial COMx?
:v0.4.4: - Instruction server functions:
           - INST|READ: dict. of all instruments as define in _dump_sensors
           - INST|LIST: dict. of all instrument tags <TAG> and their resp. class
           - INST|<TAG>|<CMD>: raw output of the <CMD> for inst <TAG>
:v0.4.4.1: - Rigol library to use TCP instead of serial (dalvarez)
:v0.4.5: - Configuration file instrumentation_config_2 json file with proper structure
         - Communication will loop over every entry and configure the communication to the instrument
         - Addint timestamp to the dictionnary of the datapoint, switching reading # and time in log files
         
		Only ONE real time process at a time. Either the monitoring from CRIMON and the only data available would be the 
		last point
		Or the instruction server is running and monitoring is not and full real time is available

		On a longer time scale, when acquiring data with less than a second DIT will not give enough time to the
		instruction server to request a whole new set of data. With this solution the response time is much faster

------------ 

:v0.5: - Dynamical graphical interface from the json file.

# Stop support for windows GUI

------------ 

:v1.0: - Porting software to command line interface (windows and UNIX) using PYVISA for communication
         with USB, GPIB, Serial and Ethernet.
       - Porting to raspberry Pi 3 with custom python environnement
       - Changing all time stamps to UTC
         - datetime.datetime.now() changed to datetime.datetime.utcnow()
       - Adding a description for values returned by the drivers
       - Adding a header in the data log file with the descriptions
       - Adding STOP/START/CLOSE/COMMENT/HELP functionnalities to IS
       
         - START/STOP: starting and stoping the monitoring of instruments
         
         - CLOSE: closing the communication and save file
         
         - COMMENT: add a comment to the log file with a timestamp
         
         - HELP: print instructions that are supported by the IS
         
         - EXIT: close client connection
         
         - INFOS: start/savepath infos
         
         - STATUS: 	monitoring status
         
       - Changing configuration file format

:v1.1: - Updating the instruction server with EXPS which allows to send several instruction with a delay of 1s

:v1.1.1: - Adding Lakeshore 218 to the lakeshores drivers (8 intputs, no outputs for heaters). 

:v1.1.2: - Adding project to Gitlab and creating sphinx documentation

:v2: - Implementing an async OPC-UA server instead of the old instruction server
     - Commands are still the same but the examples have been moved to another repo pymont-utils

