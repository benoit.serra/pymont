.. pymont documentation master file, created by
   sphinx-quickstart on Tue Apr 16 14:01:59 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pymont's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. toctree::
   :caption: Introduction
   :maxdepth: 4

   introduction/pymont
   introduction/installation
   introduction/get_started

.. toctree::
   :caption: Usage
   :maxdepth: 4
   
   usage/configuration
   usage/usecases
   usage/monitoring
   usage/contributions
   
.. toctree::
   :caption: Others
   :maxdepth: 4
  
   others/raspberry
   others/default

.. toctree::
   :caption: About
   :maxdepth: 4

   about/faq
   about/acronyms
   about/changelog
   about/license
   about/authors
   about/references
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
