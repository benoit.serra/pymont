Configuration
*************

.. _supported_instrument:

Supported Instruments
=====================

.. _lakeshore:

Lakeshore
---------

	:Models tested: 
		218, 336, 340

	:Comments:
	
		Lakeshore temperature controller are usually controlled through port 7777
		when using TCP/IP. GPIB address must be retrieved from their manual or
		in the communication settings of the controller.

	:Configuration:
	
		The lakeshores drivers are using the models field to define the
		inputs that are being queried when dumping sensors values.
		It is a dictionnary with the key being the *IDN? string output
		of the Lakeshore unit and the value being a list of the inputs.
		
		.. code-block:: bash

			  "T":{
				 "type":"Temperature",
				 "ident":[
					"Lakeshore336",
					"Lakeshore336"
				 ],
				 "comm":[
					"TCPIP::134.171.5.180::7777::SOCKET",
					"TCPIP::134.171.5.181::7777::SOCKET"
				 ],
				 "models":{
					"LSCI,MODEL336,336AANG/#######,2.2": ["A","B","C"],
					"LSCI,MODEL336,LSA163F/LSA15SI,2.7": ["B","C","D","D2","D3","D4","D5"],
					"LSCI,MODEL336,LSA22LU/LSA217G,2.8": ["A","B","C"],
					"LSCI,MODEL340,341677,042304": ["A","B"]
				 }

	:Commands:
		
		Commands are the same described in the Lakeshore manual
		
		.. code-block:: bash
			
			INST|T001|*IDN?
			> LSCI,MODEL336,336AANG/#######,2.2
			INST|T001|KRDG? 0
			> string with all temperatures

.. _agilent:

Agilent
-------

	:Models tested: 
		DSO5034A

	:Comments: 

	:Configuration: 

	:Commands: 

.. _ci-systems:

CI Systems
----------

	:Models tested: 
		SR80

	:Comments: 

	:Configuration: 

	:Commands: 

.. _pfeiffer:

Pfeiffer
--------

	:Models tested: 
	
		TPG262

	:Comments: 
	
		Models TPG 262 controllers for the vacuum gauges only have serial
		communication port available.
		
	:Configuration: 

	.. code-block:: bash

		  "P":{
			 "type":"Vacuum gauges",
			 "ident":[
				"TPG262"
			 ],
			 "comm":[
				"ASRL/dev/ttyUSB0::INSTR"
			 ],
			 "models":{

			 },
			 "config":[
				""
			 ]
		  }

	:Commands: 

	.. code-block:: bash

		TBW

.. _riello:

Riello
------

	:Models tested:
		
		UPS VSD3000

	:Comments:
	
		Models VSD3000 only have serial communication port available.

	:Configuration:
	
	.. code-block:: bash

		  "UPS":{
			 "type":"Emergency power supply",
			 "ident":[
				"UPS_VSD3x"
			 ],
			 "comm":[
				"ASRL/dev/ttyUSB1::INSTR"
			 ],
			 "models":{

			 },
			 "config":[
				""
			 ]
		  }
	
	:Commands: 

	.. code-block:: bash

		TBW

.. _rigol:

Rigol
-----

	:Models tested:

		DP831A, DG1000Z, DM3068

	:Comments:
	
		For the instrument from Rigol, the port for TCP/IP communication 
		is sometimes not mentionned. They usually use port 5555.
		If you can connect through ethernet to the instrument and you know its local
		IPv4 address, you can try going to the link below, which will display
		a raw xml file with all information from your instrument.
		http://[IPADDRESS]/lxi/identification

	:Configuration:
	
	.. code-block:: bash

		  "PSU":{
			 "type":"Power supplies",
			 "ident":[
				"DG1062Z"
			 ],
			 "comm":[
				"TCPIP::134.171.5.187::5555::SOCKET"
			 ],
			 "models":{

			 },
			 "config":[
				""
			 ]
		  }

	:Commands: 

	.. code-block:: bash

		TBW
		
.. _roheschwarze:

Rohe&Schwarze 
-------------

	:Models tested:

		HMC8012, HMP4040

	:Comments:
	
		The multimeters from R&S (HMC8012) can be accessed through port 5025.
		In the config field, it is possible to say which type of measurement
		the multimeter should set itself to [DC_I, AC_I, DC_V, AC_V].

	:Configuration:
	
	.. code-block:: bash

		  "MM":{
			 "type":"Multimeters",
			 "ident":[
				"HMC8012",
				"HMC8012"
			 ],
			 "comm":[
				"TCPIP::134.171.5.182::5025::SOCKET",
				"TCPIP::134.171.5.183::5025::SOCKET"
			 ],
			 "models":{

			 },
			 "config":[
				"DC_I",
				"DC_V"
			 ]
		  }

	:Commands: 

	.. code-block:: bash

		TBW

.. _schneider:

Schneider
---------

	:Models tested:

		MDI23

	:Comments:
	
		Motors provided by Schneider also have a dedicated software to test the
		connection. The interface to interact with the motors is sometimes a big 
		buggy and needs to be restarted using ctrl+C.
		The communication with the motors is only through Ethernet and the motors
		MUST be on a local network (192.168.[].[]). A possible solution is to use
		a NAT switch to have the IP addresses swap to ones that are on your network.
		In this case, the serial number 227170302 is for identifying the motor
		for the filter wheel.

	:Configuration:
		
	.. code-block:: bash

		  "MOT":{
			 "type":"Filter wheel",
			 "ident":[
				"FilterWheel"
			 ],
			 "comm":[
				"TCPIP::192.168.33.2::503::SOCKET"
			 ],
			 "models":{
				"227170302":["HM:0", "Filt1:117600", "Filt2:233600", "Filt3:349200"]
			 },
			 "config":[
				""
			 ]
		  }

	:Commands: 

	.. code-block:: bash

		INST|MOT001|P=0
		> Sent P=0 to MOT001
		INST|MOT001|PR P
		> 0

.. _sumitomo:

Sumitomo
--------

	:Models tested:

		F70

	:Comments:
	
		TBW
	
	:Configuration:
		
	.. code-block:: bash

		  "C":{
			 "type":"Helium Compressor",
			 "ident":[
				"COMP_F70"
			 ],
			 "comm":[
				"ASRL/dev/ttyUSB2::INSTR"
			 ],
			 "models":{

			 },
			 "config":[
				""
			 ]
		  }
		  
	:Commands:

	As per revision G of the Operating manual (September 2016 - 267472A).
	The four digit code is the checksum.
	
	.. code-block:: bash

		INST|C001|ON177CF			# Turns the compressor on
		> Sent ON177CF to C001
		INST|C001|OFF9188			# Turns the compressor off
		> Sent OFF9188 to C001
		INST|C001|TEAA4B9			# Request all temperature sensors
		> Sent TEAA4B9 to C001
		INST|C001|PRA95F7			# Request all temperature sensors
		> Sent PRA95F7 to C001
		INST|C001|STA3504			# Request the status
		> Send STA3504 to C001

.. _hgh:

HGH
---

	:Models tested:

		ECN100
		
	:Comments:
	
		On all functions in the driver, there is a dummy read to empty
		the output buffer. The reason is because when the blackbody reach
		stability, it sends the flag (True, as the output of function 92)
		to all listeners.
		
		.. note::
		
			Driver will have to be worked upon to include a passive monitoring
			where the blackbody send the stability flag when ready, and then
			we request all parameters?

	:Configuration:
	
	.. code-block:: bash

		  "BB":{
			 "type":"Blackbody",
			 "ident":[
				"ECN100"
			 ],
			 "comm":[
				"TCPIP::134.171.5.192::2101::SOCKET"
			 ],
			 "models":{

			 },
			 "config":[
				""
			 ]
		  }

	:Commands:

	.. code-block:: bash

		INST|BB001|65?			# Read the absolute setpoint (function 65 in manual)
		> +002000			# Blackbody is at 20 temperature unit [K,C,F]
		INST|BB001|65,100		# Set the absolute setpoint to 100 [K,C,F]
		> /x06				# Acknowledge

Infrared Systems Development
----------------------------

	:Models tested:

		IR301
		
	:Comments:
	
		GPIB interface seemed to be easily stalled, only modbus interfaces
		worked in a stable fashion.

	:Configuration:
	
	.. code-block:: bash

		  "BB":{
			 "type":"Blackbody controller",
			 "ident":[
				"IR301"
			 ],
			 "comm":[
				"MODBUS/dev/ttyUSB0::1::INSTR"
			 ],
			 "models":{

			 },
			 "config":[
				""
			 ]
		  }

	:Commands:

	.. code-block:: bash

		INST|BB001|100?			# Read the absolute temperature (function 65 in manual)
		> 262					# Blackbody is at 26.2 temperature unit [K,C,F]
		INST|BB001|300,100		# Set the absolute setpoint to 100 [K,C,F]
		> 100					# Returns back temperature?

.. _configuration:

Configuration file
==================

The instrument configuration can be changed in the instrumentation_config_2.json file. For each type of instrument (temperature, pressure, power supply, multimeter…) two fields are mandatory:

- Ident: which will determine which driver it will use [TBD a summary of all instrument drivers]
- Comm: the communication process and the address (serial+com or TCP+IP address)

Other optional fields will be described for each category

.. code-block:: json

	{
		"GLOBAL_CONFIG": {
			"host": "192.168.7.190:4500",
			"savepath": "/home/username/Softwares/pymont/",
			"sampling": "5",
			"timeout": "1"
		},
		"Instruments": {
			"T": {
				"type": "Temperature",
				"ident": [
					"Lakeshore336",
					"Lakeshore336"
				],
				"comm": [
					"TCPIP::192.168.7.180::7777::SOCKET",
					"TCPIP::192.168.7.181::7777::SOCKET"
				],
				"models": {
					"LSCI,MODEL336,336AANG/#######,2.2": ["A","B","C"],
					"LSCI,MODEL336,LSA163F/LSA15SI,2.7": ["B","C","D","D2","D3","D4","D5"],
					"LSCI,MODEL336,LSA22LU/LSA217G,2.8": ["A","B","C"],
					"LSCI,MODEL340,341677,042304": ["A","B"]
				},
				"config": [
					"",
					"",
					""
				]
			},
			"MM": {
				"type": "Multimeters",
				"ident": [
					"HMC8012",
					"HMC8012"
				],
				"comm": [
					"TCPIP::192.168.7.182::5025::SOCKET",
					"TCPIP::192.168.7.183::5025::SOCKET"
				],
				"models": {},
				"config": [
					"DC_I",
					"DC_V"
				]
			},
			"PSU": {
				"type": "Power supplies",
				"ident": [
					"DG1062Z"
				],
				"comm": [
					"TCPIP::192.168.7.187::5555::SOCKET"
				],
				"models": {},
				"config": [
					""
				]
			},
			"C": {
				"type": "Helium Compressor",
				"ident": [
					"COMP_F70"
				],
				"comm": [
					"ASRL/dev/ttyUSB2::INSTR"
				],
				"models": {},
				"config": [
					""
				]
			},
			"P": {
				"type": "Vacuum gauges",
				"ident": [
					"TPG262"
				],
				"comm": [
					"ASRL/dev/ttyUSB0::INSTR"
				],
				"models": {},
				"config": [
					""
				]
			}
		}
	}


