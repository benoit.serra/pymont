Adding new instruments
***********************

Every new instrument that needs to be used with the monitoring requires
some preparation on the software side.
Basic procedure would be to ask the author to take a look at the instrument
but this is the usual procedure I go through when adding a new instrument

.. _new-driver:

Writing a new driver
====================

First you need to make sure of the communication protocol and the
interface you will be using with the instrument.

For instrument that have SCPI protocol implemented, some basic commands
can be used such as \*IDN? which gives the identification of the
instrument.

For most of current instrument, just taking a driver template (in the 
drivers/ directory) and replacing the write/read termination character
should be enough for basic communication purposes.

In order to use the monitoring, the drivers should include a _dump_sensor
method which will return a dictionnary containing a list of variables and
associated values.

Below is the example written for the HMP4040 four channel power supply:

.. code-block:: python

	#### HMP 4040 #################################################################

	class HMP4040(object):
		r"""Class that implements the common driver for the model HMC 4040
		power supply. 
		* *IDN?: Identifiation query (model identification)
		"""

		def __init__(self, 
					 RESOURCE_STRING, 
					 RESOURCE_MANAGER = None,
					 RESOURCE_ID = '',
					 RESOURCE_TIMEOUT = 5,
					 TERMINATION_STRING = '\r\n',
					 **kwargs):
			
			"""Initialize internal variables and ethernet connection

			:param RESOURCE_STRING: The adress of the agilent
			:type RESOURCE_STRING: str
			:param TERMINATION_STRING: '\n' by default for agilent
			:type TERMINATION_STRING: str
			"""
			try:
				self.instrument = RESOURCE_MANAGER.open_resource(RESOURCE_STRING,
				open_timeout = RESOURCE_TIMEOUT)
				self.instrument.read_termination = TERMINATION_STRING
				self.instrument.write_termination = TERMINATION_STRING
				self.instrument.timeout = RESOURCE_TIMEOUT
				self.connected = True
			except:
				self.connected = False

			try:
				self.inst_id = RESOURCE_ID+'_'
				print (self.inst_id)
			except:
				self.inst_id = ''
			
			if RESOURCE_MANAGER == None:
				sys.exit('No VISA resource manager found')

		def fetch(self):
			"""Send a command and check if it is positively acknowledged

			:param command: The command to send
			:type command: str
			:raises IOError: if the negative acknowledged or a unknown response
				is returned
			"""   
			results = dict()
			for i in [1,2,3,4]:
				self.instrument.query('INST OUT'+str(i))
				self.mode='Volts OUT'+str(i)
				results.update({self.inst_id+self.mode:[self.instrument.query('MEAS:VOLT?'),'[V] Bias channel '+str(i)]})
				self.mode='Current OUT'+str(i)
				results.update({self.inst_id+self.mode:[self.instrument.query('MEAS:CURR?'),'[A] Current channel '+str(i)]})

			return results
			
		def dump_sensors(self):
			""""""
			results = {**self.fetch()}

			return results


.. _driver-pymont:

Including the driver in pymont
==============================

Now, we need to make sure that we know what to call when we want to add
this instrument in the configuration file.
For that, we modify the select_instrument method and add a string
which will designate the instrument in the config file and links it to
the correct class in the drivers.

Following our previous example, we need to import the rohe&schwarze drivers

.. code-block:: python

	import drivers.lakeshore.lakeshore as ls
	import drivers.pfeiffer.vacuumgauge as vg
	import drivers.roheandschwarze.roheandschwarze as rs	# This one
	import drivers.rigol.rigol as rig
	import drivers.agilent.agilent as agi
	import drivers.riello.ups_driver as riello
	import drivers.sumitomo.sumitomo as sumitomo
	import drivers.cisystem.blackbody as cibb

.. code-block:: python

	def select_instrument(identifier):
		""""""
		instruments = {'Lakeshore218':ls.LakeShore218,
                               'Lakeshore336':ls.LakeShore33x,
                               'Lakeshore340':ls.LakeShore34x,
                               'HMC4040':rs.HMC4040,	# This one
                               'HMC8012':rs.HMC8012,
                               'DP831A':rig.DP831A,
                               'TPG262':vg.TPG262,
                               'DSO5034':agi.DSO5034A,
                               'DG1062Z':rig.DG1000Z,
                               'DM3068':rig.DM3068,
                               'UPS_VSD3x':riello.UPSVSD3xxx,
                               'COMP_F70':sumitomo.COMP_F70,
                               'SR80':cibb.SR80,
                               'ECN100':hgh.ECN100}
		
		print (identifier)
		return instruments[identifier]

.. _config-file:

Changing the configuration file
===============================

.. code-block:: json

	{
	   "GLOBAL_CONFIG":{
		  "host":"192.168.7.190:4500",
		  "savepath":"/home/username/Softwares/pymont/",
		  "sampling":"5",
		  "timeout":"1"
	   },
	   "Instruments":{
		  "PSU":{
			 "type":"Power supplies",
			 "ident":[
				"HMP4040"
			 ],
			 "comm":[
				"TCPIP::192.168.7.133::5555::SOCKET"
			 ],
			 "models":{},
			 "config":[]
		  }
	   }
	}
