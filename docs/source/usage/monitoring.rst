Monitoring
**********

.. _monitoring-introduction:

Introduction
============

When pymont starts, all instruments described in the configuration JSON 
file will be connected (or it will try at the very least).
An instruction server (IS) is then started on port 4500 that can be 
accessed using the telnet protocol or through the socket library 
(Python).
Every instrument will be read every five seconds by default. If
specified in the configuration file, the sampling will be different.

.. _starting:

Staring the monitoring
======================

Starting the monitoring requires to send a start command to the instruction
server. 

Using telnet
++++++++++++

.. code-block:: bash

	$ telnet <IP_ADDRESS> <PORT>

And once the connection is established:

.. code-block:: bash

	START

Using python
++++++++++++

.. code-block:: python

	# Importing socket library
	import socket

	def InstructionClient(TCP_IP, PORT):
		""""""
		# Configure socket connection
		client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		# Connect to the server at TCP_IP:PORT
		client.connect((TCP_IP, PORT))
		client.settimeout(5)

		return client
		
	if __name__ == "__main__":
		print ('Processing...')
	 
		# Constants
		BUFFER_SIZE = 2048
		TCP_IP = '192.168.7.133'
		PORT = 4500

		# Connecting to the instruction server
		client = InstructionClient(TCP_IP, PORT)
		# Sending start intruction
		client.sendall('START\r\n'.encode())
		# Waiting for acknowledge of the instruction server
		output = client.recv(BUFFER_SIZE)

		# Print the output and print their associated type
		print (output.decode(), type(output.decode()))    

    
.. note::
	
	At this point, while connected to the instruction server other
	commands can be used...


.. _instruction-server:

Instruction Server
==================

The instruction server allows users to interact with pymont. It can
specifically interact with the monitoring to start/stop it, change the
sampling time or the save path for the data.

Some commands can be executed anytime:

:HELP:
	Print the help.
:EXIT:
	Close the current connection to the monitoring. 
:INFOS:    
	Print the starting time of the monitoring [UTC] and the save file path.
:START:
	Start the monitoring, creating a [date]_[time].txt log file with 
	the UTC time.
:STOP:     
	Stop the monitoring, closing properly the log file.
:CLOSE:
	Stop the monitoring. Then close every connection and the 
	resource manager from pyvisa and then stop the python script properly.
:INST:
	The INST command allows to access the instruments connected through
	the communication server.

	INST|LIST:
		Instruments connected and their associated TAG value
		(T001, PSU002,...).
	INST|READ:
		Reading of all connected instrument using the dump_sensors
		from each driver.
	INST|[TAG]|[COMMAND]:
		Send COMMAND to the instrument TAG, careful with the string 
		formatting, no protection as for v1.0.
:EXP:
	The EXP command allows the user to defined a set of predefined commands
	to be used.
	
	EXP|expname:
		The command will execute the list of commands expname that is defined
		in pymont/exps/[BENCHNAME]_exps.json
:COMMENT:  
	The COMMENT command allows to enter comments in the logfile 

	COMMENT|comment (str):
		Will put 'comment' in the Comments columns of the logfile, with
		an associated timestamp and empty field for other keywords.


.. warning::

	Be careful of the different uses of EXIT/STOP/CLOSE:
	
	- EXIT will close the current connection to the instruction server.
	- STOP will stop the monitoring of instrument if it is running.
	- CLOSE will stop pymont closing the monitoring, and instruments connections.

.. note::

	Others are used for configuration of the monitoring, so must be executed
	when the monitoring is not active (Status: Inactive).

	:SAMPLING:
		the SAMPLING command allows to chaning the sampling time between
		two measures.

		SAMPLING|value (int):
			Will change current sampling to value in seconds. 
	   
	:SAVEPATH:
		Change the path where the data is saved.

		SAVEPATH|path (str):
			Will change the savepath to 'path'

	:CONNECT:
		The CONNECT command will reconnect all instruments that are described
		in the JSON configuration file.
		
		CONNECT|[TAG]:
			Will connect instrument described by [TAG] (T001, PSU002...)

	:DISCONNECT:
		The DISCONNECT command will disconnect all instruments that have been
		defined in the JSON configuration file.
		
		CONNECT|[TAG]:
			Will disconnect instrument described by [TAG] (T001, PSU002...)

.. _experiments:

Starting experiments
====================

Default
