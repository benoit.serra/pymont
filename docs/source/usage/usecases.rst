Use cases
*********

.. _usecases-introduction:

Introduction
============

Pymont configuration can be as simple as a single setup or one can arrange
to have an array of devices running pymont and use the data on the
OPC servers to publish this data for live monitoring.

.. _pymonteso:

Pymont at ESO
=============

At the European Southern Observatory (ESO), several cryo-vacuum test bench
are using Pymont for control and monitoring, allowing for automation
of the setup, setup of alarms systems and live monitoring of all active
test benches.
Each test bench is equipped with a raspberry 4 connected on a ESO private
subnet, they are all running the same custom image which mean that
installation of one system can take a few hours for the simplest cases
(meaning drivers already implemented and adapted for use case, and 
lab equipment present and properly connected with working cables).

A UNIX virtual machine was created for the purpose of having a centralized
location to store and monitor all test benches. The virtual machine is
using `Prometheus <https://prometheus.io>`_ as a time series database,
and `Grafana <https://grafana.com>`_ for display of the data.

Hardware
++++++++

The hardware needed is the following:

  - Raspberry 4 + power supply
  - SD card with standard image
  - Ethernet cable
  - Instruments to connect
  - UNIX server with prometheus and grafana installed
  
Software
++++++++

For the server, Prometheus (as TSDB) and Grafana (for display) were used
, it should be also possible to use InFluxDB instead for a TSDB but it 
was not tested.

The raspberry is then using a prometheus client updating its data from
a client asking all data from instrument to the OPC server of Pymont.
By default, the data is available at the IP address of the raspberry on
port 9010 (what we call an Endpoint). For each variable the name of the
test bench was appended to it (followed by an underscore) to avoid mixing
setups data in Grafana.

The Prometheus server targets (in the prometheus.yaml configuration file)
includes all of the Endpoints (all the raspberrys monitoring the test
benches). It scrapes the data at a defined sample rate and format everything
into chunks of data. The Prometheus server's data was available at the
IP address of the machine and port 9090.

Grafana was hosted on the same machine and the Prometheus server was
set as a datasource. That gave access to all data scrapped by Prometheus
to be plotted in Grafana. Each time a new query is created, you can select
the setup and then the variable you are interested in. Once done, the
data is displayed, up to the amount of time the data from Prometheus stays
available on server (default 15 days, this can be changed). 

.. image:: ../images/pymont_diagram.png
  :width: 600
  :alt: Diagram of the software interfaces

To give an order of magnitude, with roughly 10 setups and an average of
3 instruments per setup, the total size of the Prometheus database is
227 Mb. Extending it to 30 days would then double the size.
  
For saving the actual data, we chose to record the data from the OPC server
directly since a tool was already available at ESO. One other way would
be to use the Prometheus server and request all data from a setup in 
the last day or so. 
