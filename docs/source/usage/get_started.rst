Monitoring
**********

.. _monitoring-introduction:

Introduction
============

When pymont starts, all instruments described in the configuration YAML 
configuration file will be have their connection established (or it will 
try at the very least).
The OPC server is started and every instrument dump_sensor method is used
to build the node architecture of the OPC server.
By default, the OPC server will update every node successively every one
second (plus the read out time of all instruments).

.. _starting:

Staring the monitoring
======================

Pymont does not have an internal monitoring method to record the data. 
This functionnality will be required when remote monitoring will not be
available.

Starting the prometheus client
==============================

Test


Starting the monitoring requires to send a start command to the instruction
server. 

Using telnet
++++++++++++

.. code-block:: bash

	$ telnet <IP_ADDRESS> <PORT>

And once the connection is established:

.. code-block:: bash

	START

Using python
++++++++++++

.. code-block:: python

	# Importing socket library
	import socket

	def InstructionClient(TCP_IP, PORT):
		""""""
		# Configure socket connection
		client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		# Connect to the server at TCP_IP:PORT
		client.connect((TCP_IP, PORT))
		client.settimeout(5)

		return client
		
	if __name__ == "__main__":
		print ('Processing...')
	 
		# Constants
		BUFFER_SIZE = 2048
		TCP_IP = '192.168.7.133'
		PORT = 4500

		# Connecting to the instruction server
		client = InstructionClient(TCP_IP, PORT)
		# Sending start intruction
		client.sendall('START\r\n'.encode())
		# Waiting for acknowledge of the instruction server
		output = client.recv(BUFFER_SIZE)

		# Print the output and print their associated type
		print (output.decode(), type(output.decode()))    

    
.. note::
	
	At this point, while connected to the instruction server other
	commands can be used...


.. _instruction-server:

Instruction Server
==================

The instruction server allows users to interact with pymont. It can
specifically interact with the monitoring to start/stop it, change the
sampling time or the save path for the data.

Commands
++++++++
	
	- INFOS
	- STATUS
	- START
	- STOP
	- EXPS|<EXPERIMENT_NAME>
	- SAMPLING
	- COMMENT|<COMMENT_STRING>
	
	- INST|<INSTRUMENT_TAG>|<COMMAND>
	- INST|READ
	- INST|LIST

.. _experiments:

Starting experiments
====================

Default
