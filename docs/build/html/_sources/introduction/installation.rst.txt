Installation
************

Using git
---------

.. code-block:: bash

	$ git clone https://gitlab.com/benoit.serra/pymont.git

By download
-----------

Go to the `pymont gitlab page <https://gitlab.com/benoit.serra/pymont>`_.
and download the whole repository.
